#!/usr/bin/env bash 

# Execute all files once.
if ! [ $# -eq 0 ]
then
    find -name "*.cm" -exec ./build/Parallel-FPT {} \;
else
    ./build/Parallel-FPT ./data/critical_clique_examples_k_50/graph7.cm
fi

# if ! [[ "$1" =~ ^[0-9]+$ ]]
# then
#     ./build/Parallel-FPT data/samples_with_k_2000_to_4000/random_graphs_size_random100to5000_k_2000/graph_29.cm
# else
#     # Execute the program $1 times.
#     for ((i = 1; i <= $1; i++ )); do
#         ./build/Parallel-FPT data/samples_with_k_2000_to_4000/random_graphs_size_random100to5000_k_2000/graph_29.cm
#     done
# fi
