#!/usr/bin/env bash 

# Create build directory if it does not exist
if [ ! -d "build/" ]; then
    mkdir build
fi
# Build the application
if [ "$1" = "--debug" ]
    # Debug version
    then
        echo "Creating debug build..."
        cd build
        cmake -DCMAKE_BUILD_TYPE=Debug ..
    # Release version
    else
        echo "Creating release build..."
        cd build
        cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
fi
make
cd ..
