#include "precomp.h"
#include "GPU.h"

GPU::GPU(int size, int adj_size)
{
    WriteOutput = new Kernel("src/radix-sort/radix-sort.cl", "WriteOutput");

    Return_values = new Buffer(1 * sizeof(int));
    return_values = (int *)Return_values->GetHostPtr();
    memset(return_values, 0, 1 * sizeof(int));

    Adj_list = new Buffer(adj_size * sizeof(uint));
    Vertices = new Buffer(size * sizeof(uint));
    Neighbor_counts = new Buffer(size * sizeof(uint));
    Min_index = new Buffer(size * size * sizeof(uint));
    Max_index = new Buffer(adj_size * sizeof(uint));
    Prefix_sum = new Buffer(adj_size * sizeof(uint));
    Local_totals = new Buffer(adj_size * sizeof(uint));
    Neighbor_prefix_sum = new Buffer(size * sizeof(uint));
    Neighbor_values = new Buffer(size * sizeof(uint));
    Locations = new Buffer(size * sizeof(uint));
    Output = new Buffer(adj_size * sizeof(uint));
    CC_sizes = new Buffer(size * sizeof(uint));
    CC_indices = new Buffer(size * sizeof(uint));
    CC_counts = new Buffer(size * sizeof(uint));
    Critical_cliques = new Buffer(size * sizeof(uint));
    Local_prefixsum = new Buffer(adj_size * sizeof(uint));
    SmallBuffer = new Buffer(adj_size * sizeof(uint));
    EditingDegree = new Buffer(adj_size * sizeof(uint));

    ResetVertexLocations = new Kernel("src/kernels/vertex-sort.cl", "ResetVertexLocations");
    ResetVertexLocations->SetArgument(0, Neighbor_counts);

    output = (uint *)Output->GetHostPtr();
    memset(output, 0, adj_size * sizeof(uint));
}

void GPU::SetEditingDegreeBuffer(int size)
{
    EditingDegreeEdges = new Buffer(size * size * sizeof(uint));
}

void GPU::WriteBackValues(int size, Buffer *buffer1, Buffer *buffer2)
{
    WriteOutput->SetArgument(0, buffer1);
    WriteOutput->SetArgument(1, buffer2);
    WriteOutput->Run(size);
}

void GPU::WriteBackVertices(int size, Buffer *buffer, Buffer *output, bool write_back)
{
    ResetVertexLocations->SetArgument(1, buffer);
    ResetVertexLocations->SetArgument(2, output);
    ResetVertexLocations->Run(size);
    if (write_back)
        WriteBackValues(size, buffer, output);
}

int GPU::GetReturnValue()
{
    WriteOutput->FinishCL();
    Return_values->CopyFromDevice();
    return return_values[0];
}

void GPU::CopyBuffersToDevice()
{
    Vertices->CopyToDevice();
    Adj_list->CopyToDevice();
    Neighbor_counts->CopyToDevice();
}

void GPU::PrintBuffer(Buffer *buffer, int size, int mask, int shift)
{
    FinishCL();
    WriteBackValues(size, Output, buffer);
    Output->CopyFromDevice();
    for (int i = 0; i < size; i++)
    {
        std::cout << ((output[i] & mask) >> shift) << std::endl;
    }
    std::cout << std::endl;
}

GPU::~GPU()
{
    delete output;
    delete return_values;

    // Kernels.
    delete WriteOutput;

    // Buffers.
    delete Vertices;
    delete Neighbor_counts;
    delete Adj_list;
    delete Prefix_sum;
    delete Min_index;
    delete Max_index;
    delete Local_totals;
    delete Local_prefixsum;
    delete Output;
    delete Neighbor_values;
    delete Neighbor_prefix_sum;
    delete Return_values;
    delete Locations;
    delete CC_sizes;
    delete CC_indices;
    delete CC_counts;
    delete Critical_cliques;
    delete SmallBuffer;
    delete EditingDegree;
    delete EditingDegreeEdges;
}