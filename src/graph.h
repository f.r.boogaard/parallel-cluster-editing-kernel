#include "precomp.h"
#include "timer.h"

#include "rules/rule1/rule1.h"
#include "rules/rule2/rule2.h"
#include "rules/rule3&4/rule34.h"
#include "rules/rule5/rule5.h"

class Graph
{
public:
    // Size of the graph.
    int size;
    // Size of the adjacency list.
    int adj_size;
    // The vertices of the graph. The value tells us where in `adj_list`
    // the neighbors of this vertex start.
    uint *vertices;
    // A concatenated array of all neighbors in the graph.
    // The first 16 bits contain the neighbor value.
    // The last 16 bits contain the id of the vertex.
    uint *adj_list;
    // This array contains the degree of each vertex in the first 16 bits.
    // The last 16 bits contain the vertex id.
    uint *neighbor_counts;
    // Adjacency matrix representation of the graph.
    int *adj_matrix;

    Graph(std::string file_name, bool weighted);
    ~Graph();
    // Sorts the neighbors of each vertex.
    void SortNeighbors();
    // Filters out removed edges and vertices from the graph.
    // Only call this function when you found edges that should be removed.
    bool FilterGraph(int rule);
    // Sorts the vertices based on what neighbors they have.
    void SortVertices();
    // Constructs CC graph.
    void FindCCs();
    // Calculates the editing degree of all CCs.
    void GetEditingDegree();
    // Finds the edits for the rule. Outputs the edits to std::cout if out is true.
    int FindRuleEdits(int k, int rule, bool out);
    // Prints the Adjacency matrix of the graph.
    void PrintAdjMatrix();
    // Prints the Adjacency list of the graph.
    void PrintAdjList();
    // Prints all the info we currently have about the CCs.
    void PrintCCInfo();
#if PRINT
    // Outputs the total time and the number of edits, then closes the output file.
    void CloseOutputFile(int edits, double time, int initial_k);
#endif

private:
    // Amount of bits needed to encode a single vertex id.
    int bits_per_vertex;
    // The maximum amount of vertices that can be stored in a single integer.
    int vertices_per_int;
    // The number of CCs that are in the graph.
    int cc_size;

    GPU *gpu;
    RadixSort *radix_sort;

    Kernel *SetDuplicateEdgesFlags;
    Kernel *InitVertexSort;
    Kernel *LoadDegreeValues;
    Kernel *LoadNeighborValues;
    Kernel *OrderVertices;
    Kernel *WriteVertices;
    Kernel *SetFilterValues;
    Kernel *UpdatePrefixSum;

    Kernel *FilterCCs;
    Kernel *FilterCCAdjList;
    Kernel *UpdateCCMinValues;
    Kernel *CalculateCCNeighborCounts;
    Kernel *SwapCCIdsAndCCSize;
    Kernel *SetCCNeighbors;
    Kernel *CountConnectedNeighbors;
    Kernel *CalculateEditingDegree;
    Kernel *SetVertexEditingDegree;

    uint *cc_sizes;
    uint *cc_indices;

    Rule1 *rule1;
    Rule2 *rule2;
    Rule34 *rule34;
    Rule5 *rule5;

#if PRINT
    std::ofstream outfile;
#endif

    // Filters out vertices that are in the correct spot.
    int FilterSortedVertices(int unsorted_vertices);
    // Initializes all GPU kernels and buffers and copies them to the GPU.
    void InitCL(uint *verts, uint *adjacency_list);
    // Prints the time it took for a rule to finish its calculations.
    void PrintRuleTime(Time t1, int rule_nr, int edits);
    // Writes the rule edits to std::cout
    void OutputEdits(int edits);
    // Parses a graph from a .cm file
    void ParseGraph(std::string file_name, bool weighted);
};
