#include "precomp.h"
#include "tests/test.h"
#include "graph.h"
#include "shared.h"

Graph::Graph(std::string file_name, bool weighted)
{
    ParseGraph(file_name, weighted);

    // Initialize the critical_clique values with 0.
    radix_sort->ClearFlags(size, gpu->Critical_cliques);
}

void Graph::SortNeighbors()
{
#if PRINT
    gpu->FinishCL();
    Time t1 = getCurrentTime();
#endif

    // Initialize the min_index and max_index values.
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Neighbor_prefix_sum);
    gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Output);
    radix_sort->InitNeighborSort(adj_size, gpu->Neighbor_prefix_sum, gpu->Output, gpu->Adj_list);

    // Sort the neighbors.
    radix_sort->Sort(adj_size, bits_per_vertex - 1, gpu->Adj_list);
#if PRINT
    gpu->FinishCL();
    outfile << getElapsedTime(t1) << ", ";
#endif
#if TEST
    gpu->FinishCL();
    gpu->Adj_list->CopyFromDevice();
    testRadixSortOrder(size, vertices, adj_size, adj_list);
#endif
}

bool Graph::FilterGraph(int rule)
{
#if PRINT
    gpu->FinishCL();
    Time t1 = getCurrentTime();
#endif

    switch (rule)
    {
    case 1:
        adj_size -= rule1->EditGraph(adj_size, size);
        break;
    case 2:
        adj_size -= rule2->EditGraph(adj_size, size);
        break;
    case 3:
        adj_size -= rule34->EditGraph(adj_size, size);
        break;
    case 5:
        adj_size -= rule5->EditGraph(adj_size, size);
        break;
    default:
        std::cout << "Argument exception: " << rule << " is not a valid value for rule" << std::endl;
        throw;
        break;
    }

#if TEST
    gpu->FinishCL();
    gpu->Vertices->CopyFromDevice();
    gpu->Neighbor_counts->CopyFromDevice();
    gpu->Adj_list->CopyFromDevice();
    testGraphSymmetry(size, vertices, adj_list, neighbor_counts);
#endif
#if PRINT
    gpu->FinishCL();
    outfile << adj_size << " " << getElapsedTime(t1) << ", ";
#endif

    // Every vertex will always have itself as a neighbor.
    // So if adj_size == size, then the graph is empty.
    return adj_size > size && adj_size > 1 && size > 1;
}

inline int Graph::FilterSortedVertices(int unsorted_vertices)
{
    // Put the neighbor counts and vertices in the right place.
    OrderVertices->Run(unsorted_vertices);
    WriteVertices->Run(unsorted_vertices);

    // Set bits for the vertices that are in the correct spots.
    SetFilterValues->Run(unsorted_vertices);

    // Calculate a prefix sum of the filter values and store it in Prefix_sum.
    radix_sort->CalculatePrefixSum(unsorted_vertices, gpu->Neighbor_values);

    // Correct the neighbor value prefix sum based on the amount of
    // vertices that are now in the correct spot.
    UpdatePrefixSum->Run(unsorted_vertices);

    // Write back the min and prefix sum values.
    gpu->WriteBackValues(unsorted_vertices, gpu->Neighbor_prefix_sum, gpu->Output);
    gpu->WriteBackValues(unsorted_vertices, gpu->Min_index, gpu->Locations);

    return gpu->GetReturnValue();
}

void Graph::SortVertices()
{
#if PRINT
    gpu->FinishCL();
    Time t1 = getCurrentTime();
#endif

    InitVertexSort->Run(size);

    int unsorted_vertices = size;

    // In the first round we will use the degrees on the vertices
    // to sort. The rounds after that we will use the nb values.
    LoadDegreeValues->Run(size);
    radix_sort->Sort(size, bits_per_vertex - 1, gpu->Neighbor_values, {gpu->Locations});
    unsorted_vertices -= FilterSortedVertices(unsorted_vertices);

    // Keep sorting the nb values until all vertices are sorted.
    int offset = 0;
    int bits = vertices_per_int * bits_per_vertex - 1;
    while (unsorted_vertices > 0)
    {
        // Load the next neighbor values in a single int.
        LoadNeighborValues->SetArgument(0, offset);
        LoadNeighborValues->Run(unsorted_vertices);

        // Sort the neighbor values.
        radix_sort->Sort(unsorted_vertices, bits, gpu->Neighbor_values, {gpu->Locations});

        // Retrieve the number of vertices from the GPU that are in
        // the correct spot and update the size.
        unsorted_vertices -= FilterSortedVertices(unsorted_vertices);
        offset += vertices_per_int;
    }

    // Calculate a prefix sum on the CC_indices to get the unique indices.
    radix_sort->CalculatePrefixSum(size, gpu->CC_indices);
    gpu->WriteBackValues(size, gpu->CC_indices, gpu->Prefix_sum);

    // Reorder the CC values so that we can get the CC of a value by index.
    gpu->WriteBackVertices(size, gpu->CC_indices, gpu->Output, true);
    gpu->WriteBackVertices(size, gpu->CC_sizes, gpu->Locations, true);

    // Retrieve the number of CC there are from the GPU.
#if PRINT
    gpu->FinishCL();
    outfile << getElapsedTime(t1) << ", ";
#endif
#if TEST
    gpu->FinishCL();
    gpu->Vertices->CopyFromDevice();
    gpu->Neighbor_counts->CopyFromDevice();
    gpu->Adj_list->CopyFromDevice();
    gpu->CC_sizes->CopyFromDevice();
    gpu->CC_indices->CopyFromDevice();

    testVertexSort(size, vertices, adj_list, neighbor_counts);
    testCCs(size, vertices, adj_list, neighbor_counts, cc_sizes, cc_indices);
#endif
}

int Graph::FindRuleEdits(int k, int rule, bool out)
{
#if PRINT
    gpu->FinishCL();
    Time t1 = getCurrentTime();
#endif

    int edits = 0;
    switch (rule)
    {
    case 1:
        edits = rule1->GetEdits(k, adj_size, size, cc_size, out);
        break;
    case 2:
        edits = rule2->GetEdits(k, adj_size, size, cc_size, out);
        break;
    case 3:
        edits = rule34->GetEdits(k, adj_size, size, cc_size, out);
        break;
    case 5:
        // Assumes that rule 3&4 was executed right before this function is called.
        // We know that N(K) is a single CC and that N_2(K) only has one element: u.
        edits = rule5->GetEdits(k, adj_size, size, cc_size, out);
        break;
    default:
        outfile << "Argument exception: " << rule << " is not a valid value for rule" << std::endl;
        throw;
        break;
    }

#if PRINT
    PrintRuleTime(t1, rule, edits);
#endif
    return edits;
}

void Graph::FindCCs()
{
    // Put the Vertices and Neighbor counts back in their original place.
    // This way we can construct and cc_adj_list.
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Neighbor_prefix_sum);
    gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Output);

    // Create an adjacency list for the CCs.
    FilterCCs->Run(size);
    radix_sort->CalculatePrefixSum(size, gpu->CC_counts);
    cc_size = gpu->GetReturnValue();
    SetCCNeighbors->SetArgument(0, cc_size);
    SetCCNeighbors->Run(adj_size);
    gpu->WriteBackValues(adj_size, gpu->Prefix_sum, gpu->Output);
}

void Graph::GetEditingDegree()
{
#if PRINT
    gpu->FinishCL();
    Time t1 = getCurrentTime();
#endif

    FindCCs();
    // Pass the initial cc_size to the rules.
    gpu->SetEditingDegreeBuffer(cc_size);
    SetDuplicateEdgesFlags->SetArgument(2, gpu->EditingDegreeEdges);
    CountConnectedNeighbors->SetArgument(8, gpu->EditingDegreeEdges);
    CalculateEditingDegree->SetArgument(1, gpu->EditingDegreeEdges);

    rule1->SetCCSize(cc_size);
    rule2->SetCCSize(cc_size);
    rule34->SetCCSize(cc_size);
    rule5->SetCCSize(cc_size);
    int cc_adj_size = gpu->GetReturnValue();

    // Sort the neighbors lists of the CCs
    radix_sort->InitNeighborSort(cc_adj_size, gpu->Neighbor_values, gpu->CC_counts, gpu->Max_index);
    radix_sort->Sort(cc_adj_size, bits_per_vertex - 1, gpu->Max_index);

    // Filter out duplicate CC neighbors.
    FilterCCAdjList->Run(cc_adj_size);
    cc_adj_size = radix_sort->Filter(cc_adj_size, gpu->Max_index, gpu->Min_index, gpu->Output);

    // Update the min_values, neighbor_counts and store the cc_size in the cc_adj_list.
    UpdateCCMinValues->Run(cc_adj_size);
    CalculateCCNeighborCounts->SetArgument(0, cc_adj_size);
    CalculateCCNeighborCounts->Run(cc_adj_size);
    SwapCCIdsAndCCSize->Run(cc_adj_size);

    // Filter out duplicate edges.
    SetDuplicateEdgesFlags->Run(cc_adj_size);
    gpu->WriteBackValues(cc_adj_size, gpu->EditingDegree, gpu->Output);
    int no_dup_size = radix_sort->Filter(cc_adj_size, gpu->EditingDegree, gpu->EditingDegreeEdges, gpu->SmallBuffer);
    radix_sort->Filter(cc_adj_size, gpu->Max_index, gpu->EditingDegreeEdges, gpu->Output);
    radix_sort->ClearFlags(size * size, gpu->EditingDegreeEdges);

    // Count the connected neighbors of all CCs and calculate the editing degrees for each CC.
    CountConnectedNeighbors->SetArgument(0, cc_size);
    CountConnectedNeighbors->Run(no_dup_size);
    CalculateEditingDegree->SetArgument(0, cc_size);
    CalculateEditingDegree->Run(cc_size);

    // Get the editing degree for each vertex.
    radix_sort->ClearFlags(adj_size, gpu->EditingDegree);
    SetVertexEditingDegree->Run(size);
    radix_sort->ClearFlags(adj_size, gpu->SmallBuffer);

#if PRINT
    gpu->FinishCL();
    outfile << getElapsedTime(t1) << ", " << cc_size << ", " << cc_adj_size << ", ";
#endif
}

// Pretty prints the adjacency matrix of the graph.
void Graph::PrintAdjMatrix()
{
    for (int j = 0; j < size; j++)
    {
        outfile << "----";
    }
    outfile << "-" << std::endl;

    for (int i = 0; i < size; i++)
    {
        outfile << "|";
        for (int j = 0; j < size; j++)
        {
            outfile << " " << adj_matrix[i * size + j] << " |";
        }
        outfile << std::endl;
        for (int j = 0; j < size; j++)
        {
            outfile << "----";
        }
        outfile << "-" << std::endl;
    }
    outfile << std::endl;
}

// Pretty prints the adjacency list of the graph.
void Graph::PrintAdjList()
{
    gpu->FinishCL();
    gpu->Vertices->CopyFromDevice();
    gpu->Neighbor_counts->CopyFromDevice();
    gpu->Adj_list->CopyFromDevice();
    gpu->CC_indices->CopyFromDevice();
    gpu->CC_sizes->CopyFromDevice();
    for (int i = 0; i < size; i++)
    {
        int index = vertices[i];
        int neighbors = neighbor_counts[i] & 0x0000ffff;
        int id = (neighbor_counts[i] & 0xffff0000) >> (MAX_BITS / 2);
        std::cout << id << ", " << neighbors << ", " << cc_sizes[id] << ", " << cc_indices[id] << " ||";
        for (int j = 0; j < neighbors; j++)
        {
            std::cout << " " << (adj_list[index + j] & 0x0000ffff) << " |";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Graph::PrintCCInfo()
{
    gpu->FinishCL();
    gpu->WriteBackValues(size, gpu->Output, gpu->EditingDegree);
    gpu->FinishCL();
    gpu->Output->CopyFromDevice();
    for (int i = 0; i < size; i++)
    {
        // outfile << i << ", " << gpu->output[i] << std::endl;
        outfile << gpu->output[i] << std::endl;
    }
    outfile << std::endl;
}

void Graph::PrintRuleTime(Time t1, int rule_nr, int edits)
{
    gpu->FinishCL();
    outfile << rule_nr << " " << getElapsedTime(t1) << " " << edits;
    if (rule_nr == 5 && edits == 0)
    {
        outfile << ", ";
    }
    else
    {
        outfile << " ";
    }
}

void Graph::CloseOutputFile(int edits, double time, int initial_k)
{
    outfile << initial_k << ", " << edits << ", " << time << std::endl;
    outfile.close();
}

Graph::~Graph()
{
    // Normal fields
    delete adj_list;
    delete adj_matrix;
    delete vertices;
    delete neighbor_counts;
    delete cc_sizes;
    delete cc_indices;

    // Kernels
    delete InitVertexSort;
    delete LoadNeighborValues;
    delete OrderVertices;
    delete WriteVertices;
    delete SetFilterValues;
    delete UpdatePrefixSum;
    delete FilterCCs;
    delete SetCCNeighbors;
    delete CountConnectedNeighbors;
    delete CalculateEditingDegree;
    delete SetDuplicateEdgesFlags;

    delete gpu;
    delete radix_sort;
}
