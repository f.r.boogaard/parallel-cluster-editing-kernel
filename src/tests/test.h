#include "../precomp.h"

// Tests whether all neighbors of all vertices are sorted correctly.
void testRadixSortOrder(int size, uint *vertices, int adj_size, uint *adj_list)
{
    for (int i = 0; i < size; i++)
    {
        int min_index = vertices[i];
        int max_index = i < size - 1 ? vertices[i + 1] : adj_size;
        int adj_size = max_index - min_index;
        for (int j = 0; j < adj_size - 1; j++)
        {
            if ((adj_list[min_index + j] & 0x0000ffff) < (adj_list[min_index + j + 1] & 0x0000ffff))
            {
                std::cout << "Oops! The radix sort just made a mistake!" << std::endl;
                return;
            }
        }
    }
}

// Tests whether the radix sort prefix sum works correctly.
void testPrefixSum(int bits, int adj_size, int *adj_list, int *min_indices, int *max_indices, int *prefix_sum)
{
    for (int i = 0; i < adj_size / LOCAL_WORK_SIZE; i++)
    {
        for (int j = 0; j < LOCAL_WORK_SIZE; j++)
        {
            int id = i * LOCAL_WORK_SIZE + j;
            int bit = (adj_list[id] & (1 << bits)) >> bits;
            int min_index = std::max(min_indices[id], i * LOCAL_WORK_SIZE);
            int max_index = std::min(max_indices[id], i * LOCAL_WORK_SIZE + LOCAL_WORK_SIZE - 1);
            int shift = prefix_sum[max_index] & 0x0000ffff;
            if (bit)
            {
                prefix_sum[id] = (prefix_sum[id] & 0x0000ffff) + min_index - 1;
            }
            else
            {
                prefix_sum[id] = ((prefix_sum[id] & 0xffff0000) >> (MAX_BITS / 2)) + shift + min_index - 1;
            }
        }
        bool hoi = false;
        int *test = new int[LOCAL_WORK_SIZE];
        memset(test, 0, sizeof(int) * LOCAL_WORK_SIZE);
        for (int k = 0; k < LOCAL_WORK_SIZE; k++)
        {
            if ((prefix_sum[i * LOCAL_WORK_SIZE + k] % LOCAL_WORK_SIZE) >= LOCAL_WORK_SIZE)
            {
                std::cout << prefix_sum[i * LOCAL_WORK_SIZE + k] << std::endl;
            }
            if (test[(prefix_sum[i * LOCAL_WORK_SIZE + k]) % LOCAL_WORK_SIZE] == 1)
            {
                hoi = true;
            }
            test[prefix_sum[i * LOCAL_WORK_SIZE + k] % LOCAL_WORK_SIZE] = 1;
            // std::cout << g->prefix_sum[i * LOCAL_WORK_SIZE + k] % LOCAL_WORK_SIZE << " ";
        }
        // std::cout << std::endl;
        delete test;
        if (hoi)
        {
            std::cout << "Error!!!" << std::endl;
        }
    }
}

// For now this just prints the neighbor count values.
void testNeighborValues(int size, int bits_per_vert, uint *neighbor_values)
{
    int start = (MAX_BITS / bits_per_vert) * bits_per_vert;
    for (int i = 0; i < size; i++)
    {
        uint value = neighbor_values[i];
        for (int j = start - bits_per_vert; j >= 0; j -= bits_per_vert)
        {
            std::cout << (value >> j) << " | ";
            value = (value << (MAX_BITS - j)) >> (MAX_BITS - j);
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// Tests whether the vertices are correctly sorted based on their neighbors.
void testVertexSort(int size, uint *vertices, uint *adj_list, uint *neighbor_counts)
{
    for (int i = 0; i < size - 1; i++)
    {
        int location1 = vertices[i];
        int location2 = vertices[i + 1];
        int next = false;
        int size = std::min(neighbor_counts[i] & 0x0000ffff, neighbor_counts[i + 1] & 0x0000ffff);
        // Loop over the neighbors until they are different.
        for (int j = 0; j < size; j++)
        {
            int value1 = (adj_list[location1 + j] & 0x0000ffff);
            int value2 = (adj_list[location2 + j] & 0x0000ffff);
            if (value1 < value2)
            {
                std::cout << "Oops! Vertices are not sorted correctly!" << std::endl;
                return;
            }
            if (value1 > value2 || value1 == 0)
            {
                next = true;
                break;
            }
        }
        if (next)
        {
            continue;
        }
    }
}

// Tests whether the cc_sizes and cc_indices values are correct.
void testCCs(int size, uint *vertices, uint *adj_list, uint *neighbor_counts, uint *cc_sizes, uint *cc_indices)
{
    for (int i = 0; i < size;)
    {
        int index = vertices[i];
        uint neighbors = neighbor_counts[i] & 0x0000ffff;
        int id = (neighbor_counts[i] & 0xffff0000) >> (MAX_BITS / 2);
        for (uint j = 1; j < cc_sizes[id]; j++)
        {
            int next_id = (neighbor_counts[i + j] & 0xffff0000) >> (MAX_BITS / 2);
            int next_index = vertices[i + j];
            if (cc_indices[id] != cc_indices[next_id] || (neighbor_counts[i + j] & 0x0000ffff) != neighbors)
            {
                std::cout << "Something went wrong when constructing the CC indices!" << std::endl
                          << cc_indices[id] << ", " << id << std::endl;

                return;
            }
            for (uint k = 0; k < neighbors; k++)
            {
                if ((adj_list[index + k] & 0x0000ffff) != (adj_list[next_index + k] & 0x0000ffff))
                {
                    std::cout << "Something went wrong when constructing the CC sizes!" << std::endl;
                    return;
                }
            }
        }
        i += cc_sizes[id];
    }
}

// Tests when an edge (u,v) is in the graph whether (v,u) is also present.
void testGraphSymmetry(int size, uint *vertices, uint *adj_list, uint *neighbor_counts)
{
    for (int i = 0; i < size; i++)
    {
        uint vert_id = neighbor_counts[i] >> (MAX_BITS / 2);
        int nb_count = neighbor_counts[i] & 0x0000ffff;
        int start = vertices[i];
        // if start is 0, then the vertex is removed from the graph.
        if (start != 0)
        {
            for (int j = 0; j < nb_count; j++)
            {
                int nb = adj_list[start + j] & 0x0000ffff;
                for (int k = 0; k < size; k++)
                {
                    int vert_id2 = neighbor_counts[k] >> (MAX_BITS / 2);
                    if (vert_id2 == nb)
                    {
                        int nb_count2 = neighbor_counts[k] & 0x0000ffff;
                        int start2 = vertices[k];
                        bool found = false;
                        for (int l = 0; l < nb_count2; l++)
                        {
                            if ((adj_list[start2 + l] & 0x0000ffff) == vert_id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            std::cout << "Oops, an edge is missing from the graph!" << std::endl
                                      << vert_id << ", " << nb << std::endl;
                            return;
                        }
                    }
                }
            }
        }
    }
}

// Prints the first `size` elements of and array.
void print(uint *array, int size, int mask = 0xffffffff)
{
    for (int i = 0; i < size; i++)
    {
        std::cout << (array[i] & mask) << std::endl;
    }
    std::cout << std::endl;
}

// Prints the first `size` elements of and array.
void print(ushort *array, int size)
{
    for (int i = 0; i < size; i++)
    {
        std::cout << array[i] << std::endl;
    }
    std::cout << std::endl;
}
