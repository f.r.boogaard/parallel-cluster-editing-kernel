#define LOCAL_WORK_SIZE 256        // Local work size used by the GPU.
#define MAX_BITS (sizeof(int) * 8) // 2^16, we use this to store two values in a single int.

#define PRINT_OUTPUT 1 // Print the edits to std::cout
#define PRINT 1        // Write timing data to an output file (see graph.h)
#define TEST 0         // Runs a few tests while running the program

// NOTE: Most test probably dont work anymore after we
// added the updates of the editing degree and
// critical cliques (see tests/test.h).
