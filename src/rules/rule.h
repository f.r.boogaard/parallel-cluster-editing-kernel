#ifndef RULE_H_ // Make sure that this file
#define RULE_H_ // can only be included once.

#include "../precomp.h"
#include "../radix-sort/radix-sort.h"

class Rule
{
public:
    Rule(GPU *gpu, RadixSort *radix_sort) : gpu(gpu), radix_sort(radix_sort)
    {
        UpdateNeighborCounts = new Kernel("src/kernels/vertex-sort.cl", "UpdateNeighborCounts");
        UpdateNeighborCounts->SetArgument(0, gpu->Adj_list);
        UpdateNeighborCounts->SetArgument(1, gpu->Vertices);
        UpdateNeighborCounts->SetArgument(2, gpu->Neighbor_counts);
        UpdateNeighborCounts->SetArgument(3, gpu->Prefix_sum);
        UpdateNeighborCounts->SetArgument(4, gpu->CC_indices);
        UpdateNeighborCounts->SetArgument(5, gpu->CC_counts);
        UpdateNeighborCounts->SetArgument(6, gpu->Critical_cliques);

        SetVertexRemoveFlags = new Kernel("src/rules/rule.cl", "SetVertexRemoveFlags");
        SetVertexRemoveFlags->SetArgument(0, gpu->Neighbor_counts);
        SetVertexRemoveFlags->SetArgument(1, gpu->Neighbor_values);
        SetVertexRemoveFlags->SetArgument(2, gpu->CC_indices);
        SetVertexRemoveFlags->SetArgument(3, gpu->CC_counts);
        SetVertexRemoveFlags->SetArgument(4, gpu->Max_index);
        SetVertexRemoveFlags->SetArgument(5, gpu->Local_prefixsum);
        SetVertexRemoveFlags->SetArgument(6, gpu->SmallBuffer);
        SetVertexRemoveFlags->SetArgument(7, gpu->Output);

        WriteBackCCSizes = new Kernel("src/rules/rule.cl", "WriteBackCCSizes");
        WriteBackCCSizes->SetArgument(0, gpu->CC_indices);
        WriteBackCCSizes->SetArgument(1, gpu->CC_sizes);
        WriteBackCCSizes->SetArgument(2, gpu->Max_index);

        SetDoubleEdgesFlags = new Kernel("src/rules/rule.cl", "SetDoubleEdgesFlags");
        SetDoubleEdgesFlags->SetArgument(0, gpu->Adj_list);
        SetDoubleEdgesFlags->SetArgument(1, gpu->Neighbor_values);
        SetDoubleEdgesFlags->SetArgument(2, gpu->CC_indices);
        SetDoubleEdgesFlags->SetArgument(3, gpu->Max_index);

        SetRemovedEdgesFlags = new Kernel("src/rules/rule.cl", "SetRemovedEdgesFlags");
        SetRemovedEdgesFlags->SetArgument(0, gpu->Adj_list);
        SetRemovedEdgesFlags->SetArgument(1, gpu->Min_index);
        SetRemovedEdgesFlags->SetArgument(2, gpu->SmallBuffer);
        SetRemovedEdgesFlags->SetArgument(3, gpu->Neighbor_values);
        SetRemovedEdgesFlags->SetArgument(4, gpu->Max_index);

        UpdateEditingDegreeEdges = new Kernel("src/rules/rule.cl", "UpdateEditingDegreeEdges");
        UpdateEditingDegreeEdges->SetArgument(1, gpu->Output);
        UpdateEditingDegreeEdges->SetArgument(2, gpu->Adj_list);
        UpdateEditingDegreeEdges->SetArgument(3, gpu->Neighbor_prefix_sum);
        UpdateEditingDegreeEdges->SetArgument(4, gpu->Local_totals);
        UpdateEditingDegreeEdges->SetArgument(6, gpu->CC_indices);
        UpdateEditingDegreeEdges->SetArgument(7, gpu->Critical_cliques);
        UpdateEditingDegreeEdges->SetArgument(8, gpu->Locations);
        UpdateEditingDegreeEdges->SetArgument(9, gpu->SmallBuffer);
        UpdateEditingDegreeEdges->SetArgument(10, gpu->Neighbor_values);

        ChangeEditingDegree = new Kernel("src/rules/rule.cl", "ChangeEditingDegree");
        ChangeEditingDegree->SetArgument(1, gpu->Output);
        ChangeEditingDegree->SetArgument(2, gpu->EditingDegree);
        ChangeEditingDegree->SetArgument(3, gpu->Max_index);

        UpdateEditingDegreeValues = new Kernel("src/rules/rule.cl", "UpdateEditingDegreeValues");
        UpdateEditingDegreeValues->SetArgument(1, gpu->Adj_list);
        UpdateEditingDegreeValues->SetArgument(2, gpu->Locations);
        UpdateEditingDegreeValues->SetArgument(3, gpu->Neighbor_prefix_sum);
        UpdateEditingDegreeValues->SetArgument(4, gpu->CC_indices);
        UpdateEditingDegreeValues->SetArgument(6, gpu->EditingDegree);

        FlagChangedCriticalCliques = new Kernel("src/rules/rule.cl", "FlagChangedCriticalCliques");
        FlagChangedCriticalCliques->SetArgument(1, gpu->Adj_list);
        FlagChangedCriticalCliques->SetArgument(2, gpu->CC_indices);
        FlagChangedCriticalCliques->SetArgument(4, gpu->Neighbor_prefix_sum);

        ChangeCCIndices = new Kernel("src/rules/rule.cl", "ChangeCCIndices");
        ChangeCCIndices->SetArgument(1, gpu->Adj_list);
        ChangeCCIndices->SetArgument(2, gpu->CC_indices);
        ChangeCCIndices->SetArgument(4, gpu->Neighbor_prefix_sum);

        UpdateCCSizes = new Kernel("src/rules/rule.cl", "UpdateCCSizes");
        UpdateCCSizes->SetArgument(0, gpu->Adj_list);
        UpdateCCSizes->SetArgument(1, gpu->CC_indices);
        UpdateCCSizes->SetArgument(2, gpu->CC_sizes);
        UpdateCCSizes->SetArgument(3, gpu->CC_counts);
        UpdateCCSizes->SetArgument(4, gpu->Critical_cliques);

        SetChangedVerticesFlags = new Kernel("src/rules/rule.cl", "SetChangedVerticesFlags");
        SetChangedVerticesFlags->SetArgument(0, gpu->Locations);
        SetChangedVerticesFlags->SetArgument(1, gpu->Neighbor_values);
        SetChangedVerticesFlags->SetArgument(2, gpu->Neighbor_prefix_sum);
    };
    ~Rule()
    {
        delete UpdateNeighborCounts;
        delete SetVertexRemoveFlags;
        delete SetDoubleEdgesFlags;

        delete SetRemovedEdgesFlags;
        delete UpdateEditingDegreeEdges;
        delete ChangeEditingDegree;
        delete UpdateEditingDegreeValues;

        delete FlagChangedCriticalCliques;
        delete ChangeCCIndices;
        delete UpdateCCSizes;
    };
    // Sets the max critical clique size. This value will be
    // used to find the editing degree values in the
    // EditingDegreeEdges buffer.
    virtual void SetCCSize(int size)
    {
        UpdateEditingDegreeEdges->SetArgument(5, gpu->EditingDegreeEdges);
        UpdateEditingDegreeValues->SetArgument(5, gpu->EditingDegreeEdges);
        FlagChangedCriticalCliques->SetArgument(3, gpu->EditingDegreeEdges);
        ChangeCCIndices->SetArgument(3, gpu->EditingDegreeEdges);

        cc_size = size;
        bits_per_cc = 1;
        int v = size;
        while (v >>= 1)
        {
            bits_per_cc++;
        }
    };
    // Return the number of edits the rule found. If `output` is true these
    // edits are written to std::cout.
    virtual int GetEdits(int k, int adj_size, int size, int cc_size, bool print) = 0;
    // Edits the graph in GPU memory according to the edits found
    // with the GetEdits function. This function will also remove/add
    // other edges from the graph that are redundant.
    virtual int EditGraph(int adj_size, int size) = 0;

protected:
    int cc_size;
    int bits_per_cc;

    GPU *gpu;
    RadixSort *radix_sort;

    // Filters out edges from the graph. `gpu->Min_index` should
    // contain flags for the edges that should be removed.
    int FilterGraph(int adj_size, int size);
    // Writes the edits to std::cout. Removed edges should be in gpu->Output.
    // Added vertices should be in gpu->Locations.
    void PrintEdits(int removed, int size, bool print_added = true);
    // Calculates the sum of the difference between the size of a CC and the
    // size of a neighbor. If you add the number of edges that will be removed
    // from the graph, you get the number of edges that are added to the graph.
    int GetTotalNeighborSizeDifference(int size, int adj_size);

    // Update the Editing degree of all the vertices and update the
    // critical clique values. If the editing degree becomes zero
    // between two vertices, then they have the same neighbors.
    void UpdateEditingDegreeVertices(int adj_size, int size);

private:
    Kernel *UpdateNeighborCounts;

    Kernel *SetVertexRemoveFlags;
    Kernel *SetDoubleEdgesFlags;
    Kernel *WriteBackCCSizes;
    Kernel *SetTotalConnectedNeighborCount;

    Kernel *SetRemovedEdgesFlags;
    Kernel *UpdateEditingDegreeEdges;
    Kernel *ChangeEditingDegree;
    Kernel *UpdateEditingDegreeValues;

    Kernel *FlagChangedCriticalCliques;
    Kernel *ChangeCCIndices;
    Kernel *UpdateCCSizes;

    Kernel *SetChangedVerticesFlags;

    // Updates the Editing Degree of all the vertices based on the edits
    // found by the rules.
    void UpdateEditingDegree(int adj_size, int size);
};

#endif // RULE_H_
