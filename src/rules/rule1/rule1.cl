#include "../../shared.h"

// Flag an edge if it is adjacent to a CC that has more than k vertices.
__kernel void SetBigCCFlags(int k, global ushort *adj_list,
                            global uint *cc_sizes, global uint *cc_indices,
                            global uint *flags) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  if (cc_sizes[vertex] > k) {
    flags[neighbor] = cc_indices[vertex];
  }
}

// Flag the edges that will be removed from the graph.
// We only include the edits here. This kernel is used
// to output all the edits.
__kernel void SetRemoveFlags(global ushort *adj_list, global uint *flags,
                             global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  output[id] = flags[vertex] != flags[neighbor];
}

// Flag all edges that we can remove from the graph.
// This also include edges within the cliques that do not count as edits.
// This kernel is used to remove edges from memory.
__kernel void SetCliqueRemoveFlags(global ushort *adj_list, global uint *flags,
                                   global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  output[id] =
      (flags[neighbor] != 0 || flags[vertex] != 0) && neighbor != vertex;
}
