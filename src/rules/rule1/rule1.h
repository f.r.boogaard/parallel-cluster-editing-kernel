#ifndef RULE1_H_ // Make sure that this file
#define RULE1_H_ // can only be included once.

#include "../rule.h"

class Rule1 : public Rule
{
public:
    Rule1(GPU *gpu, RadixSort *radix_sort) : Rule(gpu, radix_sort)
    {
        SetBigCCFlags = new Kernel("src/rules/rule1/rule1.cl", "SetBigCCFlags");
        SetBigCCFlags->SetArgument(1, gpu->Adj_list);
        SetBigCCFlags->SetArgument(2, gpu->CC_sizes);
        SetBigCCFlags->SetArgument(3, gpu->CC_indices);
        SetBigCCFlags->SetArgument(4, gpu->Neighbor_values); // Same buffer as the 1st argument of the SetRemoveFlags kernel.

        SetRemoveFlags = new Kernel("src/rules/rule1/rule1.cl", "SetRemoveFlags");
        SetRemoveFlags->SetArgument(0, gpu->Adj_list);
        SetRemoveFlags->SetArgument(1, gpu->Neighbor_values); // Same buffer as the 4th argument of the SetBigCCFlags kernel.
        SetRemoveFlags->SetArgument(2, gpu->Min_index);

        SetCliqueRemoveFlags = new Kernel("src/rules/rule1/rule1.cl", "SetCliqueRemoveFlags");
        SetCliqueRemoveFlags->SetArgument(0, gpu->Adj_list);
        SetCliqueRemoveFlags->SetArgument(1, gpu->Neighbor_values);
        SetCliqueRemoveFlags->SetArgument(2, gpu->Min_index);
    };
    ~Rule1()
    {
        delete SetBigCCFlags;
        delete SetRemoveFlags;
        delete SetCliqueRemoveFlags;
    };
    // Return the number of edits the rule found. If `output` is true these
    // edits are written to std::cout.
    int GetEdits(int k, int adj_size, int size, int cc_size, bool print);
    // Edits the graph in GPU memory according to the edits found
    // with the GetEdits function. This function will also remove/add
    // other edges from the graph that are redundant.
    int EditGraph(int adj_size, int size);

protected:
    Kernel *SetRemoveFlags;
    Kernel *SetCliqueRemoveFlags;
    Kernel *SetVertexRemoveFlags;

private:
    Kernel *SetBigCCFlags;
};

#endif // RULE1_H_
