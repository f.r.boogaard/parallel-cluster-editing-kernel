#include "rule1.h"

int Rule1::GetEdits(int k, int adj_size, int size, int cc_size, bool print)
{
    // Flag all edge that are adjacent to a CC that has more than k vertices.
    radix_sort->ClearFlags(size, gpu->Neighbor_values);
    SetBigCCFlags->SetArgument(0, k);
    SetBigCCFlags->Run(adj_size);

    // Flag all edges that should be removed from the graph.
    SetRemoveFlags->Run(adj_size);

    // This has to be calculated before the remove_edits, otherwise gpu->Output
    // wont have the correct values.
    int add_edits = GetTotalNeighborSizeDifference(size, adj_size);

    // Place all edges that should be removed next to each other in an array.
    int remove_edits = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Min_index, gpu->Output) / 2;
    int total_edits = remove_edits + (add_edits + remove_edits) / 2;

    if (print && total_edits > 0)
    {
        PrintEdits(remove_edits * 2, size);
    }

    return total_edits;
}

int Rule1::EditGraph(int adj_size, int size)
{
    // Edit the graph.
    SetCliqueRemoveFlags->Run(adj_size);
    int edits = FilterGraph(adj_size, size);

    // Update the Critical Cliques.
    if (edits > 0 && cc_size > 0)
        UpdateEditingDegreeVertices(adj_size - edits, size);
    return edits;
}
