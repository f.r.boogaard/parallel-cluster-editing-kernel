#include "rule.h"

int Rule::FilterGraph(int adj_size, int size)
{
    // First update the editing degree of all vertices.
    if (cc_size != 0)
    {
        UpdateEditingDegree(adj_size, size);
    }
    int edits = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Min_index, gpu->Output, true);
    gpu->WriteBackValues(adj_size, gpu->Adj_list, gpu->Output);
    UpdateNeighborCounts->Run(size);
    return edits;
}

void Rule::UpdateEditingDegree(int adj_size, int size)
{
    // Filter out the edges that are removed, but
    // connected to a vertex that stays in the graph.
    SetRemovedEdgesFlags->Run(adj_size);
    int removed_edges = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Max_index, gpu->Output);
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Neighbor_prefix_sum);
    gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Local_totals);
    if (removed_edges > 0)
    {
        // Update the editing degrees.
        UpdateEditingDegreeEdges->SetArgument(0, cc_size);
        UpdateEditingDegreeEdges->Run(removed_edges);
    }
}

void Rule::UpdateEditingDegreeVertices(int adj_size, int size)
{
    // Update the editing degree for the vertices.
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Neighbor_prefix_sum);
    gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Locations);
    UpdateEditingDegreeValues->SetArgument(0, cc_size);
    UpdateEditingDegreeValues->Run(size);

    // Update the CC_indices.
    radix_sort->ClearFlags(size, gpu->Neighbor_prefix_sum);
    FlagChangedCriticalCliques->SetArgument(0, cc_size);
    FlagChangedCriticalCliques->Run(adj_size);
    ChangeCCIndices->SetArgument(0, cc_size);
    ChangeCCIndices->Run(adj_size);

    // Update the CC_sizes.
    UpdateCCSizes->Run(size);
}

int Rule::GetTotalNeighborSizeDifference(int size, int adj_size)
{
    // Find the number of edges that lie between 2 vertices that will be removed.
    SetDoubleEdgesFlags->Run(adj_size);
    int double_count_edges = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Max_index, gpu->Output) / 2;

    // Find the vertices that will be removed from the graph
    radix_sort->ClearFlags(size, gpu->Local_prefixsum);
    radix_sort->ClearFlags(size, gpu->SmallBuffer);
    radix_sort->ClearFlags(size, gpu->Output);
    WriteBackCCSizes->Run(size);
    SetVertexRemoveFlags->Run(size);

    // Sum over all edges that are not connected to the CCs for
    // which a rule applies and that are connected to a vertex
    // in N(K). If we substract the nr of removed
    // edges, we get the amount of edges present in N(K).
    radix_sort->CalculatePrefixSum(size, gpu->SmallBuffer);
    int size_diff = radix_sort->GetLargestPrefixSumValue(size);
    radix_sort->ClearFlags(size, gpu->SmallBuffer);

    // Calculate the nr of edges that should be present in N(K).
    radix_sort->CalculatePrefixSum(size, gpu->Output);
    int connected_count = radix_sort->GetLargestPrefixSumValue(size);

    // The nr of edges we should add will now be equal to:
    // connected_count - (size_diff - (removed + double_count_edges))
    // if we rewite this to the value we are returning. We only have
    // to add the removed value, which we will calculate later.

    return connected_count - size_diff + double_count_edges;
}

void Rule::PrintEdits(int removed, int size, bool print_added)
{
    // Get all the vertices that were flagged.
    int added = 0;
    if (print_added)
    {
        SetChangedVerticesFlags->Run(size);
        added = radix_sort->Filter(size, gpu->Locations, gpu->Neighbor_prefix_sum, gpu->Max_index);
    }
    if (removed > 0)
    {
        gpu->FinishCL();
        gpu->Output->CopyFromDevice();
    }
    if (added > 0)
    {
        // Start getting the added edges while we print the removed edges.
        gpu->WriteBackValues(size, gpu->Output, gpu->Max_index);
    }
    ushort *out = (ushort *)gpu->output;
    if (removed > 0)
    {
        std::cout << "Remove: " << std::endl;
        for (int i = 0; i < removed; i++)
        {
            std::cout << "(" << out[i * 2] << ", " << out[i * 2 + 1] << ")" << std::endl;
        }
    }
    if (added > 0)
    {
        std::cout << "Make clique: " << std::endl;
        gpu->FinishCL();
        gpu->Output->CopyFromDevice();
        out = (ushort *)gpu->output;
        for (int i = 0; i < added; i++)
        {
            std::cout << out[i * 2] << std::endl;
        }
    }
    std::cout << std::endl;
}
