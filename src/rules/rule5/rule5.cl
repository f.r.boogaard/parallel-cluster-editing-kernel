#include "../../shared.h"

// Flag an edge if it is connected to a u vertex.
__kernel void FlagNeighborEdgesU(global ushort *adj_list,
                                 global uint *neighbor_u, global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  // Make sure that the vertex that is stored in the edge, is not removed.
  output[id] = neighbor_u[vertex] && neighbor_u[neighbor] && vertex != neighbor;
}

__kernel void CorrectPrefixSum(global ushort *adj_list, global uint *prefix_sum,
                               global uint *vertices) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int pref_index = max(0, (int)vertices[vertex] - 1);
  prefix_sum[id] -= prefix_sum[pref_index];
}

// Flag the first |K| neighbors of a vertex that is connected to u.
// These will be removed from the graph.
__kernel void FlagRemoveVertices(global ushort *adj_list, global uint *nb_flags,
                                 global uint *prefix_sum, global uint *cc_sizes,
                                 global uint *cc_counts, global uint *cc_flags,
                                 global uint *cc_indices, global uint *big_nb_u,
                                 global uint *vertices, global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int cc_id = cc_flags[vertex] & 0x0000ffff;
  // Since changing the pointers for rule 3&4 is
  // non deterministic, we have to figure out what
  // is was when we changed it.
  int big = big_nb_u[cc_id];
  int big_pointer = adj_list[vertices[big] * 2 + 1];
  if (nb_flags[id] && vertex == big_pointer) {
    // The CC is only connected to this CC, so the size
    // is equal to the amount of neighbors minus the size of this CC.
    int cc_size =
        cc_counts[(cc_flags[vertex] & 0x0000ffff) - 1] - cc_sizes[vertex];
    // Only flag the first |K| vertices in N(K).
    output[neighbor] = prefix_sum[id] <= cc_size;
  }
}

__kernel void FlagEditedEdges(global ushort *adj_list,
                              global uint *vert_remove_flags,
                              global uint *cc_flags, global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  output[id] =
      vert_remove_flags[neighbor] && cc_flags[vertex] != cc_flags[neighbor];
}

// Flag all the edges that we now can remove from memory.
__kernel void FlagRemoveEdges5(global ushort *adj_list,
                               global uint *vert_remove_flags,
                               global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  output[id] = vert_remove_flags[neighbor] || vert_remove_flags[vertex];
}

// Point all the removed vertices to the 0 vertex.
__kernel void AdjustPointers5(int cc_size, global uint *vertices,
                              global uint *neighbor_counts,
                              global uint *vert_remove_flags,
                              global uint *cc_indices, global uint *cc_sizes) {
  int id = get_global_id(0);
  int vertex = neighbor_counts[id] >> (MAX_BITS / 2);
  if (vertex != 0 && vert_remove_flags[vertex]) {
    vertices[id] = 0;
    cc_indices[vertex] = cc_size;
    cc_sizes[vertex] = 1;
    neighbor_counts[id] =
        1 | (vertex << (MAX_BITS / 2)); // neighbor_counts[id] & 0x0000ffff;
  }
}

// Updates the editing degrees for the edges connected to u.
__kernel void
UpdateEditingDegree(int cc_size, global ushort *adj_list,
                    global uint *neighbor_u, global uint *u_flags,
                    global uint *vert_flags, global uint *cc_counts,
                    global uint *cc_indices, global uint *critical_cliques,
                    global uint *cc_sizes, global uint *editing_degree_edges) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int vert_flag = vert_flags[vertex] & 0x0000ffff;
  int nb_flag = vert_flags[neighbor] & 0x0000ffff;
  int vert_cc = cc_indices[vertex];
  int nb_cc = cc_indices[neighbor] - 1;
  int size_cc = cc_sizes[adj_list[critical_cliques[vert_flag - 1] * 2 + 1]];
  // We removed |K| nbs of u so we have to substract it from the count.
  int nb_u_count = (u_flags[vert_flag] >> (MAX_BITS / 2)) - size_cc;
  int u_cc = u_flags[vert_flag] & 0x0000ffff;
  vert_cc -= 1;

  // Vertices connected to u.
  if (vert_flag != nb_flag && neighbor_u[vertex]) {
    int pk = cc_counts[vert_flag - 1] - nb_u_count + cc_counts[u_cc - 1] -
             nb_u_count - 1;
    editing_degree_edges[nb_cc * cc_size + vert_cc] = pk;
    editing_degree_edges[vert_cc * cc_size + nb_cc] = pk;
  }
}