#include "rule5.h"

// Assumes that rule 3&4 was executed right before this function is called.
// We know that N(K) is a single CC and that N_2(K) only has one element: u.
int Rule5::GetEdits(int k, int adj_size, int size, int cc_size, bool print)
{
    // Get the original order of the vertices and neighbor_counts.
    gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Max_index);
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Output);

    // Find the vertices that are connected to u.
    radix_sort->ClearFlags(cc_size, gpu->Local_prefixsum);
    radix_sort->ClearFlags(size, gpu->Locations);
    FindBiggestNeighborU->Run(adj_size);

    // Flag an edge if it is connected to u.
    FlagNeighborEdgesU->Run(adj_size);

    // Calculate a prefix sum over those flags, so that we
    // can pick the first |K| later.
    radix_sort->CalculatePrefixSum(adj_size, gpu->Output);
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Min_index);
    CorrectPrefixSum->Run(adj_size);

    // Flag the first |K| neighbors that are connected to u using the prefixsum.
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Min_index);
    radix_sort->ClearFlags(adj_size, gpu->Max_index);
    FlagRemoveVertices->Run(adj_size);

    // Flag the edges that we should remove.
    FlagEditedEdges->Run(adj_size);
    int remove_edits = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Min_index, gpu->Output);

    if (print && remove_edits > 0)
        PrintEdits(remove_edits, size, false);

    return remove_edits;
}

int Rule5::EditGraph(int adj_size, int size)
{
    // Flag the removed edges and adjust pointers for rule 5.
    FlagRemoveEdges5->Run(adj_size);
    AdjustPointers5->SetArgument(0, cc_size);
    AdjustPointers5->Run(size);

    // Find the edits.
    int edits = FilterGraph(adj_size, size);

    // Update the Critical Cliques.
    if (edits > 0)
    {
        // update the edges connected to u and not to the cc.
        // then only recalculate the editing degrees for the vertices
        UpdateEditingDegree->SetArgument(0, cc_size);
        UpdateEditingDegree->Run(adj_size);
        UpdateEditingDegreeVertices(adj_size - edits, size);
    }
    return edits;
}
