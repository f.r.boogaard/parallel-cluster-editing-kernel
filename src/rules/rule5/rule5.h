#include "../rule.h"

class Rule5 : public Rule
{
public:
    Rule5(GPU *gpu, RadixSort *radix_sort) : Rule(gpu, radix_sort)
    {
        // TODO: this is the same kernel as the kernel in rule34
        FindBiggestNeighborU = new Kernel("src/rules/rule3&4/rule34.cl", "FindBiggestNeighborU");
        FindBiggestNeighborU->SetArgument(0, gpu->Adj_list);
        FindBiggestNeighborU->SetArgument(1, gpu->SmallBuffer);
        FindBiggestNeighborU->SetArgument(2, gpu->CC_indices);
        FindBiggestNeighborU->SetArgument(3, gpu->Max_index);
        FindBiggestNeighborU->SetArgument(4, gpu->Neighbor_values);
        FindBiggestNeighborU->SetArgument(5, gpu->Local_prefixsum);
        FindBiggestNeighborU->SetArgument(6, gpu->Locations);

        FlagNeighborEdgesU = new Kernel("src/rules/rule5/rule5.cl", "FlagNeighborEdgesU");
        FlagNeighborEdgesU->SetArgument(0, gpu->Adj_list);
        FlagNeighborEdgesU->SetArgument(1, gpu->Locations);
        FlagNeighborEdgesU->SetArgument(2, gpu->Output);

        CorrectPrefixSum = new Kernel("src/rules/rule5/rule5.cl", "CorrectPrefixSum");
        CorrectPrefixSum->SetArgument(0, gpu->Adj_list);
        CorrectPrefixSum->SetArgument(1, gpu->Prefix_sum);
        CorrectPrefixSum->SetArgument(2, gpu->Min_index);

        FlagRemoveVertices = new Kernel("src/rules/rule5/rule5.cl", "FlagRemoveVertices");
        FlagRemoveVertices->SetArgument(0, gpu->Adj_list);
        FlagRemoveVertices->SetArgument(1, gpu->Output);
        FlagRemoveVertices->SetArgument(2, gpu->Prefix_sum);
        FlagRemoveVertices->SetArgument(3, gpu->CC_sizes);
        FlagRemoveVertices->SetArgument(4, gpu->CC_counts);
        FlagRemoveVertices->SetArgument(5, gpu->Neighbor_values);
        FlagRemoveVertices->SetArgument(6, gpu->CC_indices);
        FlagRemoveVertices->SetArgument(7, gpu->Local_prefixsum);
        FlagRemoveVertices->SetArgument(8, gpu->Min_index);
        FlagRemoveVertices->SetArgument(9, gpu->Max_index);

        FlagEditedEdges = new Kernel("src/rules/rule5/rule5.cl", "FlagEditedEdges");
        FlagEditedEdges->SetArgument(0, gpu->Adj_list);
        FlagEditedEdges->SetArgument(1, gpu->Max_index);
        FlagEditedEdges->SetArgument(2, gpu->Neighbor_values);
        FlagEditedEdges->SetArgument(3, gpu->Min_index);

        FlagRemoveEdges5 = new Kernel("src/rules/rule5/rule5.cl", "FlagRemoveEdges5");
        FlagRemoveEdges5->SetArgument(0, gpu->Adj_list);
        FlagRemoveEdges5->SetArgument(1, gpu->Max_index);
        FlagRemoveEdges5->SetArgument(2, gpu->Min_index);

        AdjustPointers5 = new Kernel("src/rules/rule5/rule5.cl", "AdjustPointers5");
        AdjustPointers5->SetArgument(1, gpu->Vertices);
        AdjustPointers5->SetArgument(2, gpu->Neighbor_counts);
        AdjustPointers5->SetArgument(3, gpu->Max_index);
        AdjustPointers5->SetArgument(4, gpu->CC_indices);
        AdjustPointers5->SetArgument(5, gpu->CC_sizes);

        UpdateEditingDegree = new Kernel("src/rules/rule5/rule5.cl", "UpdateEditingDegree");
        UpdateEditingDegree->SetArgument(1, gpu->Adj_list);
        UpdateEditingDegree->SetArgument(2, gpu->Locations);
        UpdateEditingDegree->SetArgument(3, gpu->SmallBuffer);
        UpdateEditingDegree->SetArgument(4, gpu->Neighbor_values);
        UpdateEditingDegree->SetArgument(5, gpu->CC_counts);
        UpdateEditingDegree->SetArgument(6, gpu->CC_indices);
        UpdateEditingDegree->SetArgument(7, gpu->Critical_cliques);
        UpdateEditingDegree->SetArgument(8, gpu->CC_sizes);
    };
    ~Rule5()
    {
        delete FindBiggestNeighborU;

        delete FlagNeighborEdgesU;
        delete CorrectPrefixSum;
        delete FlagRemoveVertices;
        delete FlagEditedEdges;

        delete FlagRemoveEdges5;
        delete AdjustPointers5;
    };
    // Assumes that rule 3&4 was executed right before this function is called.
    // We know that N(K) is a single CC and that N_2(K) only has one element: u.
    // Return the number of edits the rule found. If `output` is true these
    // edits are written to std::cout.
    int GetEdits(int k, int adj_size, int size, int cc_size, bool print);
    // Edits the graph in GPU memory according to the edits found
    // with the GetEdits function. This function will also remove/add
    // other edges from the graph that are redundant.
    int EditGraph(int adj_size, int size);

    void SetCCSize(int size)
    {
        UpdateEditingDegree->SetArgument(9, gpu->EditingDegreeEdges);
        Rule::SetCCSize(size);
    };

private:
    Kernel *FindBiggestNeighborU;

    Kernel *FlagNeighborEdgesU;
    Kernel *CorrectPrefixSum;
    Kernel *FlagRemoveVertices;
    Kernel *FlagEditedEdges;

    Kernel *FlagRemoveEdges5;
    Kernel *AdjustPointers5;
    Kernel *UpdateEditingDegree;
};
