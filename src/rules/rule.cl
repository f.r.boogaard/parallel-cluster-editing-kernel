#include "../shared.h"

// Flag all vertices that will be removed from the graph.
__kernel void
SetVertexRemoveFlags(global uint *neighbor_counts, global uint *vert_flags,
                     global uint *cc_indices, global uint *cc_counts,
                     global uint *cc_sizes, global uint *output,
                     global uint *size_output, global uint *count_output) {
  int id = get_global_id(0);
  int vertex = neighbor_counts[id] >> (MAX_BITS / 2);
  int degree = neighbor_counts[id] & 0x0000ffff;
  int flag = vert_flags[vertex] & 0x0000ffff;
  int cc_id = cc_indices[vertex];
  uint cc_count = cc_counts[flag - 1];
  uint cc_size = cc_sizes[flag];
  int size_diff = degree - cc_size - 1;
  // Whether this vertex will be removed and the size difference with the CC.
  if (flag != 0 && cc_id != flag) {
    output[id] = 1;
    size_output[id] = size_diff;
    count_output[id] = cc_count - cc_size - 1;
  }
}

// Create an array with the cc_sizes for each cc.
// Creating such an array when constructing the ccs
// would be better, but for not this will have to do.
__kernel void WriteBackCCSizes(global uint *cc_indices, global uint *cc_sizes,
                               global uint *output) {
  int id = get_global_id(0);
  output[cc_indices[id]] = cc_sizes[id];
}

// Counts the amount of edges that need to be counted twice,
// because they lie between two different CCs that are being removed.
__kernel void SetDoubleEdgesFlags(global ushort *adj_list,
                                  global uint *vert_flags,
                                  global uint *cc_indices,
                                  global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  output[id] = vert_flags[vertex] != 0 && vert_flags[neighbor] != 0 &&
               vert_flags[vertex] != vert_flags[neighbor];
}

// Set flags to filter out edges that are being removed with
// one endpoint that will stay in the graph.
__kernel void SetRemovedEdgesFlags(global ushort *adj_list,
                                   global uint *remove_flags,
                                   global uint *big_nb_cc,
                                   global uint *cc_flags, global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  // If the cc_flags is 0, then the vertex will not be removed.
  // If the cc_flags do not match and there is a big neighbor u
  // connected to the neighbors of the cc, then it is also not removed.
  int vertex_flag = cc_flags[vertex] & 0x0000ffff;
  int nb_flag = cc_flags[neighbor] & 0x0000ffff;
  output[id] = remove_flags[id] && vertex_flag == 0;
}

// Updates the editing degree based on the edges that are being
// removed from the graph.
// TODO: filter the adj_list used in the kernel above to only
// have one edge per cc, then substract one for every uncommon nb.
__kernel void UpdateEditingDegreeEdges(
    int cc_size, global short *filtered_adj_list, global uint *adj_list,
    global uint *vertices, global uint *neighbor_counts,
    global uint *editing_degree_edges, global uint *cc_indices,
    global uint *critical_cliques, global uint *neighbor_u,
    global uint *u_flags, global uint *vertex_flags) {
  int id = get_global_id(0);
  int neighbor = filtered_adj_list[id * 2];
  int vertex = filtered_adj_list[id * 2 + 1];
  int start1 = vertices[neighbor];
  int start2 = vertices[vertex];
  int cc_size1 = neighbor_counts[neighbor] & 0x0000ffff;
  int cc_size2 = neighbor_counts[vertex] & 0x0000ffff;
  uint cc_id1 = cc_indices[vertex] - 1;
  uint nb_flag = vertex_flags[neighbor] & 0x0000ffff;
  uint nb_u = neighbor_u[neighbor];
  int i = 0;
  int j = 0;
  while (true) {
    if (i >= cc_size1 || j >= cc_size2)
      break;
    uint a = adj_list[start1 + i] & 0x0000ffff;
    uint b = adj_list[start2 + j] & 0x0000ffff;
    i += (a >= b);
    j += (a <= b);
    // If b is an uncommon nb, we have to decrement its editing degree.
    if (a < b) {
      int cc_id2 = cc_indices[b] - 1;
      if (vertices[b] == critical_cliques[cc_id2]) {
        atomic_dec(&editing_degree_edges[cc_id1 * cc_size + cc_id2]);
        atomic_dec(&editing_degree_edges[cc_id2 * cc_size + cc_id1]);
      }
      // If both vertices are connected to the u vertex, then we have to
      // increment the editing degree.
    } else if (nb_u && a == b &&
               cc_indices[a] == (u_flags[nb_flag] & 0x0000ffff)) {
      int cc_id2 = cc_indices[b] - 1;
      if (vertices[b] == critical_cliques[cc_id2]) {
        atomic_inc(&editing_degree_edges[cc_id1 * cc_size + cc_id2]);
        atomic_inc(&editing_degree_edges[cc_id2 * cc_size + cc_id1]);
      }
    }
  }
  // Decrement the remaining bs as well.
  for (int k = j; k < cc_size2; k++) {
    uint b = adj_list[start2 + k] & 0x0000ffff;
    int cc_id2 = cc_indices[b] - 1;
    if (vertices[b] == critical_cliques[cc_id2]) {
      atomic_dec(&editing_degree_edges[cc_id1 * cc_size + cc_id2]);
      atomic_dec(&editing_degree_edges[cc_id2 * cc_size + cc_id1]);
    }
  }
}

__kernel void UpdateEditingDegreeValues(int cc_size, global uint *adj_list,
                                        global uint *neighbor_counts,
                                        global uint *vertices,
                                        global uint *cc_indices,
                                        global uint *editing_degree_edges,
                                        global uint *editing_degree) {
  int id = get_global_id(0);
  int start = vertices[id];
  int size = neighbor_counts[id] & 0x0000ffff;
  int vertex = neighbor_counts[id] >> (MAX_BITS / 2);
  int cc_id1 = cc_indices[vertex] - 1;
  uint pk = 0;
  for (int i = start; i < start + size; i++) {
    int neighbor = adj_list[i] & 0x0000ffff;
    if (neighbor != vertex) {
      int cc_id2 = cc_indices[neighbor] - 1;
      pk += editing_degree_edges[cc_id1 * cc_size + cc_id2];
    }
  }
  if (size == 1)
    editing_degree[id] = 0;
  else
    editing_degree[id] = pk;
}

__kernel void FlagChangedCriticalCliques(int cc_size, global ushort *adj_list,
                                         global uint *cc_indices,
                                         global uint *editing_degree_edges,
                                         global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int cc_id1 = cc_indices[neighbor] - 1;
  int cc_id2 = cc_indices[vertex] - 1;
  int pk = editing_degree_edges[cc_id1 * cc_size + cc_id2];
  // Flag the vertex if we should merge it and the
  // cc id of the neighbor is bigger. This way the vertex
  // with the smallest id we should merge will be the
  // only one that will not get a flag.
  if (pk == 0 && cc_id1 != cc_id2 && cc_id1 < cc_id2)
    output[vertex] = 1;
}

__kernel void ChangeCCIndices(int cc_size, global ushort *adj_list,
                              global uint *cc_indices,
                              global uint *editing_degree_edges,
                              global uint *change_flags) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int cc_id1 = cc_indices[neighbor] - 1;
  int cc_id2 = cc_indices[vertex] - 1;
  int pk = editing_degree_edges[cc_id1 * cc_size + cc_id2];
  int flag = change_flags[vertex];
  if (!flag && pk == 0 && cc_id1 != cc_id2) {
    cc_indices[neighbor] = cc_indices[vertex];
  }
}

__kernel void UpdateCCSizes(global uint *adj_list, global uint *cc_indices,
                            global uint *cc_sizes, global uint *cc_counts,
                            global uint *critical_cliques) {
  int id = get_global_id(0);
  int cc = cc_indices[id];
  int count = cc_counts[cc - 1];
  int start = critical_cliques[cc - 1];
  int size = 0;
  for (int i = start; i < start + count; i++) {
    int nb = adj_list[i] & 0x0000ffff;
    if (cc_indices[nb] == cc)
      size++;
  }
  cc_sizes[id] = size;
}

__kernel void ChangeEditingDegree(int size, global short *filtered_adj_list,
                                  global uint *editing_degree,
                                  global uint *editing_degree_updates) {
  for (int i = 0; i < size; i++) {
    int vertex = filtered_adj_list[i * 2 + 1];
    int neighbor = filtered_adj_list[i * 2];
    editing_degree[vertex] -= editing_degree_updates[i];
  }
}

__kernel void SetChangedVerticesFlags(global uint *locations,
                                      global uint *vert_flags,
                                      global uint *flags_out) {
  int id = get_global_id(0);
  locations[id] = id;
  flags_out[id] = vert_flags[id] > 0;
}
