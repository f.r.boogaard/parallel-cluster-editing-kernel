#include "../rule.h"

class Rule34 : public Rule
{
public:
    Rule34(GPU *gpu, RadixSort *radix_sort) : Rule(gpu, radix_sort)
    {
        FlagApplicableCCs = new Kernel("src/rules/rule3&4/rule34.cl", "FlagApplicableCCs");
        FlagApplicableCCs->SetArgument(0, gpu->EditingDegree);
        FlagApplicableCCs->SetArgument(1, gpu->CC_indices);
        FlagApplicableCCs->SetArgument(2, gpu->CC_sizes);
        FlagApplicableCCs->SetArgument(3, gpu->CC_counts);
        FlagApplicableCCs->SetArgument(4, gpu->Adj_list);
        FlagApplicableCCs->SetArgument(5, gpu->Critical_cliques);
        FlagApplicableCCs->SetArgument(6, gpu->SmallBuffer);

        RemoveNeighboringApplicableCCs = new Kernel("src/rules/rule3&4/rule34.cl", "RemoveNeighboringApplicableCCs");
        RemoveNeighboringApplicableCCs->SetArgument(0, gpu->SmallBuffer);
        RemoveNeighboringApplicableCCs->SetArgument(1, gpu->CC_indices);
        RemoveNeighboringApplicableCCs->SetArgument(2, gpu->CC_sizes);
        RemoveNeighboringApplicableCCs->SetArgument(3, gpu->Adj_list);

        FlagEditingDegree3 = new Kernel("src/rules/rule3&4/rule34.cl", "FlagEditingDegree3");
        FlagEditingDegree3->SetArgument(0, gpu->SmallBuffer);
        FlagEditingDegree3->SetArgument(1, gpu->CC_indices);
        FlagEditingDegree3->SetArgument(2, gpu->Adj_list);
        FlagEditingDegree3->SetArgument(3, gpu->Neighbor_values);

        FilterBigCCs = new Kernel("src/rules/rule3&4/rule34.cl", "FilterBigCCs");
        FilterBigCCs->SetArgument(0, gpu->Neighbor_values);
        FilterBigCCs->SetArgument(1, gpu->Locations);

        FindNeighboringVertex = new Kernel("src/rules/rule3&4/rule34.cl", "FindNeighboringVertex");
        FindNeighboringVertex->SetArgument(1, gpu->Max_index);
        FindNeighboringVertex->SetArgument(2, gpu->CC_indices);
        FindNeighboringVertex->SetArgument(3, gpu->CC_counts);
        FindNeighboringVertex->SetArgument(4, gpu->Critical_cliques);
        FindNeighboringVertex->SetArgument(5, gpu->Adj_list);
        FindNeighboringVertex->SetArgument(6, gpu->Neighbor_values);
        FindNeighboringVertex->SetArgument(7, gpu->Min_index);
        FindNeighboringVertex->SetArgument(8, gpu->SmallBuffer);

        GetUCount = new Kernel("src/rules/rule3&4/rule34.cl", "GetUCount");
        GetUCount->SetArgument(1, gpu->Min_index);
        GetUCount->SetArgument(2, gpu->SmallBuffer);
        GetUCount->SetArgument(3, gpu->Neighbor_values);

        FlagRemoveEdges = new Kernel("src/rules/rule3&4/rule34.cl", "FlagRemoveEdges");
        FlagRemoveEdges->SetArgument(0, gpu->Adj_list);
        FlagRemoveEdges->SetArgument(1, gpu->Neighbor_values);
        FlagRemoveEdges->SetArgument(2, gpu->SmallBuffer);
        FlagRemoveEdges->SetArgument(3, gpu->CC_indices);
        FlagRemoveEdges->SetArgument(4, gpu->Min_index);
        FlagRemoveEdges->SetArgument(5, gpu->Output);

        FindBiggestNeighborU = new Kernel("src/rules/rule3&4/rule34.cl", "FindBiggestNeighborU");
        FindBiggestNeighborU->SetArgument(0, gpu->Adj_list);
        FindBiggestNeighborU->SetArgument(1, gpu->SmallBuffer);
        FindBiggestNeighborU->SetArgument(2, gpu->CC_indices);
        FindBiggestNeighborU->SetArgument(3, gpu->Max_index);
        FindBiggestNeighborU->SetArgument(4, gpu->Neighbor_values);
        FindBiggestNeighborU->SetArgument(5, gpu->Local_prefixsum);
        FindBiggestNeighborU->SetArgument(6, gpu->Locations);

        AdjustPointers3 = new Kernel("src/rules/rule3&4/rule34.cl", "AdjustPointers3");
        AdjustPointers3->SetArgument(0, gpu->Neighbor_counts);
        AdjustPointers3->SetArgument(1, gpu->Max_index);
        AdjustPointers3->SetArgument(2, gpu->Vertices);
        AdjustPointers3->SetArgument(3, gpu->Output);
        AdjustPointers3->SetArgument(4, gpu->Critical_cliques);
        AdjustPointers3->SetArgument(5, gpu->CC_counts);
        AdjustPointers3->SetArgument(6, gpu->Locations);
        AdjustPointers3->SetArgument(7, gpu->Neighbor_values);
        AdjustPointers3->SetArgument(8, gpu->SmallBuffer);
        AdjustPointers3->SetArgument(9, gpu->Local_prefixsum);
        AdjustPointers3->SetArgument(10, gpu->CC_indices);
        AdjustPointers3->SetArgument(11, gpu->Adj_list);

        SetRemoveFlags3 = new Kernel("src/rules/rule3&4/rule34.cl", "SetRemoveFlags3");
        SetRemoveFlags3->SetArgument(0, gpu->Adj_list);
        SetRemoveFlags3->SetArgument(1, gpu->Local_prefixsum);
        SetRemoveFlags3->SetArgument(2, gpu->Locations);
        SetRemoveFlags3->SetArgument(3, gpu->Critical_cliques);
        SetRemoveFlags3->SetArgument(4, gpu->CC_indices);
        SetRemoveFlags3->SetArgument(5, gpu->Neighbor_values);
        SetRemoveFlags3->SetArgument(6, gpu->SmallBuffer);
        SetRemoveFlags3->SetArgument(7, gpu->Min_index);

        TestCCFlags = new Kernel("src/rules/rule3&4/rule34.cl", "TestCCFlags");
        TestCCFlags->SetArgument(0, gpu->Neighbor_values);
        TestCCFlags->SetArgument(1, gpu->Adj_list);
        TestCCFlags->SetArgument(2, gpu->Vertices);
        TestCCFlags->SetArgument(3, gpu->Neighbor_counts);
        TestCCFlags->SetArgument(4, gpu->CC_indices);
        TestCCFlags->SetArgument(5, gpu->Return_values);

        FlagUEdges = new Kernel("src/rules/rule3&4/rule34.cl", "FlagUEdges");
        FlagUEdges->SetArgument(0, gpu->Neighbor_values);
        FlagUEdges->SetArgument(1, gpu->Adj_list);
        FlagUEdges->SetArgument(2, gpu->SmallBuffer);

        FindNeighboringVertex2 = new Kernel("src/rules/rule3&4/rule34.cl", "FindNeighboringVertex2");
        FindNeighboringVertex2->SetArgument(0, gpu->SmallBuffer);
        FindNeighboringVertex2->SetArgument(1, gpu->Min_index);
        FindNeighboringVertex2->SetArgument(2, gpu->Adj_list);
        FindNeighboringVertex2->SetArgument(3, gpu->Neighbor_prefix_sum);

        UpdateEditingDegrees = new Kernel("src/rules/rule3&4/rule34.cl", "UpdateEditingDegrees");
        UpdateEditingDegrees->SetArgument(2, gpu->SmallBuffer);
        UpdateEditingDegrees->SetArgument(3, gpu->Locations);
        UpdateEditingDegrees->SetArgument(4, gpu->Adj_list);
        UpdateEditingDegrees->SetArgument(5, gpu->Neighbor_values);
        UpdateEditingDegrees->SetArgument(6, gpu->CC_indices);
        UpdateEditingDegrees->SetArgument(7, gpu->CC_counts);
    };
    ~Rule34()
    {
        delete FlagEditingDegree3;
        delete FilterBigCCs;
        delete FindNeighboringVertex;
        delete FlagRemoveEdges;

        delete FindBiggestNeighborU;
        delete AdjustPointers3;
        delete SetRemoveFlags3;
        delete TestCCFlags;
    };
    // Return the number of edits the rule found. If `output` is true these
    // edits are written to std::cout.
    int GetEdits(int k, int adj_size, int size, int cc_size, bool print);
    // Edits the graph in GPU memory according to the edits found
    // with the GetEdits function. This function will also remove/add
    // other edges from the graph that are redundant.
    int EditGraph(int adj_size, int size);

    void SetCCSize(int size)
    {
        UpdateEditingDegrees->SetArgument(1, gpu->EditingDegreeEdges);
        Rule::SetCCSize(size);
    };

private:
    Kernel *FlagApplicableCCs;
    Kernel *RemoveNeighboringApplicableCCs;
    Kernel *FlagEditingDegree3;
    Kernel *FilterBigCCs;
    Kernel *FindNeighboringVertex;
    Kernel *FlagRemoveEdges;
    Kernel *GetUCount;

    Kernel *FindBiggestNeighborU;
    Kernel *AdjustPointers3;
    Kernel *SetRemoveFlags3;
    Kernel *UpdateEditingDegrees;

    Kernel *FlagUEdges;
    Kernel *FindNeighboringVertex2;

    Kernel *TestCCFlags;

    // Updates the editing degrees for the edges that stay in the graph
    // that were adjacent to an 4-applicable critical clique.
    void UpdateEditingDegree(int adj_size, int size);
};
