#include "../../shared.h"

// Flag all the CCs when they are 3- or 4- applicable.
__kernel void FlagApplicableCCs(global uint *editing_degree,
                                global uint *cc_indices, global uint *cc_sizes,
                                global uint *cc_counts, global ushort *adj_list,
                                global uint *critical_cliques,
                                global uint *flags) {
  int vertex = get_global_id(0);
  int cc_id = cc_indices[vertex];
  int size = cc_sizes[vertex];
  int neighbor_size = cc_counts[cc_id - 1] - size;
  if (critical_cliques[cc_id] != 0 &&
      (size + neighbor_size > editing_degree[vertex]) && size < neighbor_size) {
    flags[cc_id] = cc_id | ((size + neighbor_size) << (MAX_BITS / 2));
  }
}

// If two neighboring CCs both have a flag then
// we remove the flag of the smaller CC.
__kernel void RemoveNeighboringApplicableCCs(global uint *cc_flags,
                                             global uint *cc_indices,
                                             global uint *cc_sizes,
                                             global ushort *adj_list) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int vert_cc = cc_indices[vertex];
  int nb_cc = cc_indices[neighbor];
  if (vert_cc != nb_cc && cc_flags[vert_cc] != 0 && cc_flags[nb_cc] != 0) {
    if (cc_sizes[vertex] > cc_sizes[neighbor]) {
      cc_flags[nb_cc] = 0;
    } else {
      cc_flags[vert_cc] = 0;
    }
  }
}

// Flags the vertices based on rule 3.
__kernel void FlagEditingDegree3(global uint *cc_flags, global uint *cc_indices,
                                 global ushort *adj_list, global uint *flags) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int flag = cc_flags[cc_indices[vertex]];
  if (flag) {
    flags[neighbor] = flag;
  }
}

// Sets flags to filter out all the CCs with |K| < |N(K)|
__kernel void FilterBigCCs(global uint *flags1, global uint *flags2) {
  int id = get_global_id(0);
  flags2[id] = (flags1[id] >> (MAX_BITS / 2)) != 0;
}

// Finds the neighboring vertex u for each CC, if it is there.
__kernel void FindNeighboringVertex(int global_size, global uint *vert_ids,
                                    global uint *cc_indices,
                                    global uint *cc_counts,
                                    global uint *critical_cliques,
                                    global uint *adj_list, global uint *flags,
                                    global uint *counts, global uint *flags2) {
  int id = get_global_id(0);
  int vert_id = vert_ids[id];
  int cc_id = flags[vert_id] & 0x0000ffff;        // the flag cc_id.
  int limit = (flags[vert_id] >> (MAX_BITS / 2)); // |K| + |N(K)| (of the flag)
  int vert_cc_id = cc_indices[vert_id];           // cc_id of the vert itself.
  int start = critical_cliques[vert_cc_id - 1];
  int size = cc_counts[vert_cc_id - 1];
  int offset = cc_id * global_size;
  for (int i = start; i < start + size; i++) {
    int neighbor = adj_list[i] & 0x0000ffff;
    int nb_cc_id = cc_indices[neighbor];
    if ((flags[neighbor] & 0x0000ffff) != cc_id) {
      // Update the count for the cc this vertex is flagged with.
      int value = atomic_inc(&counts[offset + nb_cc_id]);
      // Limit should be divided by two. We multiply the value by two
      // instead to match it without losing accuracy.
      if (((value + 1) << 1) > limit) {
        flags2[cc_id] = nb_cc_id;
      }
    }
  }
}

__kernel void GetUCount(int global_size, global uint *counts,
                        global uint *flags, global uint *vert_flags) {
  int cc_id = get_global_id(0);
  int u_cc = flags[cc_id];
  // if the cc has an u vertex, then add the count to the value.
  if (u_cc != 0) {
    flags[cc_id] |= counts[cc_id * global_size + u_cc] << (MAX_BITS / 2);
  }
}

// Flag all the edges that will be removed according to this rule.
__kernel void FlagRemoveEdges(global ushort *adj_list, global uint *flags,
                              global uint *big_nb_flags,
                              global uint *cc_indices, global uint *output,
                              global uint *connected_u) {
  int id = get_global_id(0);
  int vertex = adj_list[id * 2 + 1];
  int neighbor = adj_list[id * 2];
  int vertex_flag = flags[vertex] & 0x0000ffff;
  int neighbor_flag = flags[neighbor] & 0x0000ffff;
  int big_nb_vert = big_nb_flags[vertex_flag] & 0x0000ffff;
  int big_nb_nb = big_nb_flags[neighbor_flag] & 0x0000ffff;
  output[id] =
      (vertex_flag != neighbor_flag && big_nb_vert != cc_indices[neighbor] &&
       big_nb_nb != cc_indices[vertex]) ||
      // If they both have a non-zero flag we can always remove the edge.
      (vertex_flag != 0 && neighbor_flag != 0 && vertex_flag != neighbor_flag);
  // Also set a flag for the edges between u and K.
  connected_u[id] = big_nb_nb == cc_indices[vertex];
}

// Finds the highest degree neighbor that is connected to u.
// And flags all vertices that are connected to u.
__kernel void
FindBiggestNeighborU(global ushort *adj_list, global uint *cc_flags,
                     global uint *cc_indices, global uint *neighbor_counts,
                     global uint *vert_flags, global uint *big_nb_output,
                     global uint *nb_u_output) {
  int id = get_global_id(0);
  int vertex = adj_list[id * 2 + 1];
  int neighbor = adj_list[id * 2];
  int cc_id = cc_indices[neighbor];
  // If the neighbor is connected to one more vertex than the CC,
  // we know that it is connected to all neighbors of this vertex.
  if (cc_flags[cc_id] && (neighbor_counts[vertex] & 0x0000ffff) - 1 ==
                             (neighbor_counts[neighbor] & 0x0000ffff)) {
    big_nb_output[cc_id] = vertex;
  }
  // Flag all vertices that are a neighbor of u.
  int cc_clique = vert_flags[neighbor] & 0x0000ffff;
  int flag = cc_flags[cc_clique] & 0x0000ffff;
  if (flag != 0 && flag == cc_indices[vertex]) {
    nb_u_output[neighbor] = 1;
  }
}

// Change the pointers of the twin vertices.
__kernel void
AdjustPointers3(global uint *neighbor_counts, global uint *unsorted_counts,
                global uint *vertices, global uint *unsorted_vertices,
                global uint *critical_cliques, global uint *cc_counts,
                global uint *neighbor_u, global uint *vert_flags,
                global uint *cc_flags, global uint *big_nb_flags,
                global uint *cc_indices, global ushort *adj_list) {
  int id = get_global_id(0);
  int vertex = neighbor_counts[id] >> (MAX_BITS / 2);
  int cc_clique = vert_flags[vertex] & 0x0000ffff;
  // Only adjust the pointers if Rule 4 applies.
  // And if is has not been changed already in a previous rule application.
  if (cc_flags[cc_clique] && adj_list[vertices[id] * 2 + 1] == vertex) {
    // If the vertex is connected to u, then point
    // to the vertex that is connected to everything
    // in K and N(K).
    if (neighbor_u[vertex]) {
      int new_pointer = big_nb_flags[cc_clique];
      vertices[id] = unsorted_vertices[new_pointer];
      neighbor_counts[id] = (neighbor_counts[id] & 0xffff0000) |
                            (unsorted_counts[new_pointer] & 0x0000ffff);
      cc_indices[vertex] = cc_indices[new_pointer];
    } else {
      // Point to one of the vertices in K otherwise.
      vertices[id] = critical_cliques[cc_clique - 1];
      neighbor_counts[id] =
          (neighbor_counts[id] & 0xffff0000) | cc_counts[cc_clique - 1];
      cc_indices[vertex] = cc_clique;
    }
  }
}

// Flags the edges that should be removed according to rule 3 & 4
__kernel void
SetRemoveFlags3(global ushort *adj_list, global uint *big_nb_flags,
                global uint *neighbor_u, global uint *critical_cliques,
                global uint *cc_indices, global uint *vert_flags,
                global uint *cc_flags, global uint *remove_flags) {
  int id = get_global_id(0);
  int vertex = adj_list[id * 2 + 1];
  int neighbor = adj_list[id * 2];
  int vert_flag = vert_flags[vertex];
  int nb_flag = vert_flags[neighbor] & 0x0000ffff;
  int cc_clique = vert_flag & 0x0000ffff;
  int big = big_nb_flags[cc_clique];
  int u = cc_flags[cc_clique] & 0x0000ffff;
  // Rule 4.
  if (u) {
    // Neighbors of u.
    if (neighbor_u[vertex]) {
      remove_flags[id] = big != vertex;
    } else {
      remove_flags[id] =
          adj_list[critical_cliques[cc_clique - 1] * 2 + 1] != vertex;
    }
  }
  // Rule 3
  else if (cc_clique != 0 && cc_clique == nb_flag) {
    remove_flags[id] = vertex != neighbor;
  }
}

// Tests whether everything is flagged correctly.
// If a vertex is being flagged by more than one neighbor
// then this kernel should give an error.
__kernel void TestCCFlags(global uint *vert_flags, global uint *adj_list,
                          global uint *vertices, global uint *counts,
                          global uint *cc_indices, global int *return_values) {
  int id = get_global_id(0);
  int start = vertices[id];
  int vertex = counts[id] >> (MAX_BITS / 2);
  int flag = vert_flags[vertex] & 0x0000ffff;
  if (flag != 0 && cc_indices[vertex] == flag) {
    for (int i = start; i < start + (counts[id] & 0x0000ffff); i++) {
      int nb = adj_list[i] & 0x0000ffff;
      if ((vert_flags[nb] & 0x0000ffff) != flag) {
        return_values[0] = -1;
      }
    }
  }
}

////////////////////////////////////////
// Kernel that help find the u vertex //
////////////////////////////////////////

// Flag all the edges with the flags of their endpoints.
__kernel void FlagUEdges(global uint *vert_flags, global uint *adj_list,
                         global uint *output) {
  int id = get_global_id(0);
  int neighbor = adj_list[id] & 0x0000ffff;
  // For each adjacent edge we set its flag as the value.
  // We will sort these values to find out to how many
  // of the same flag this vertex is connected.
  output[id] = vert_flags[neighbor];
}

__kernel void FindNeighboringVertex2(global uint *edge_flags,
                                     global uint *min_values,
                                     global ushort *adj_list,
                                     global uint *output) {
  int id = get_global_id(0);
  int vertex = adj_list[id * 2 + 1];
  int neighbor = adj_list[id * 2];
  int cc = edge_flags[id];
  // We stored the |K| + |N(K)| value in the flag.
  int cc_size = cc >> (MAX_BITS / 2);
  int cc_id = cc & 0x0000ffff;
  int occurences = min_values[id];
  occurences = (occurences & 0x0000ffff) + (occurences >> (MAX_BITS / 2));
  // Set the vertex as a flag for the CC.
  if (occurences > (cc_size / 2)) {
    output[cc_id] = vertex;
  }
}

//////////////////////////////////////////////
// Update the editing degrees for this rule //
//////////////////////////////////////////////

__kernel void
UpdateEditingDegrees(int cc_size, global uint *editing_degree_edges,
                     global uint *u_flags, global uint *neighbor_u,
                     global ushort *adj_list, global uint *vert_flags,
                     global uint *cc_indices, global uint *cc_counts) {
  int id = get_global_id(0);
  int vertex = adj_list[id * 2 + 1];
  int neighbor = adj_list[id * 2];
  int vert_flag = vert_flags[vertex] & 0x0000ffff;
  int nb_flag = vert_flags[neighbor] & 0x0000ffff;
  int vert_cc = cc_indices[vertex];
  int nb_u_count = u_flags[vert_flag] >> (MAX_BITS / 2);
  int u_cc = u_flags[vert_flag] & 0x0000ffff;
  vert_cc -= 1;
  int nb_cc = cc_indices[neighbor] - 1;
  // Inner edge
  if (vert_flag != 0 && vert_flag == nb_flag) {
    // Both are connected to u. Or neither are connected to u.
    if ((neighbor_u[vertex] && neighbor_u[neighbor]) ||
        (!neighbor_u[vertex] && !neighbor_u[neighbor])) {
      editing_degree_edges[vert_cc * cc_size + nb_cc] = 0;
      editing_degree_edges[nb_cc * cc_size + vert_cc] = 0;
    } else {
      editing_degree_edges[vert_cc * cc_size + nb_cc] = 1;
      editing_degree_edges[nb_cc * cc_size + vert_cc] = 1;
    }
  }
  // Vertices connected to u.
  // set editing degree to cc_count - u_count + u_nb_count - u_count.
  else if (vert_flag != nb_flag && neighbor_u[vertex]) {
    int pk = cc_counts[vert_flag - 1] - nb_u_count + cc_counts[u_cc - 1] -
             nb_u_count - 1;
    editing_degree_edges[nb_cc * cc_size + vert_cc] = pk;
    editing_degree_edges[vert_cc * cc_size + nb_cc] = pk;
  }
}
