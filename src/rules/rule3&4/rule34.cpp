#include "rule34.h"

int Rule34::GetEdits(int k, int adj_size, int size, int cc_size, bool print)
{
    radix_sort->ClearFlags(size, gpu->Neighbor_values);
    radix_sort->ClearFlags(size, gpu->SmallBuffer);

    // Flag all ccs that are 3- or 4-applicable.
    FlagApplicableCCs->Run(size);

    // Remove a flag of a cc when it is adjacent to another cc with a flag.
    RemoveNeighboringApplicableCCs->Run(adj_size);

    // Flag the vertices that are adjacent to a CC for which the rule applies.
    FlagEditingDegree3->Run(adj_size);

    // Calculate the nr of edges that will be added. For Rule 3&4
    // we have to add the removed edges, but we first have to
    // make sure that we also count the edges between u and K.
    int add_edits = GetTotalNeighborSizeDifference(size, adj_size);

    // Filter out CCs with |K| < |N(K)|. This will always be the
    // case for vertices with a flag, however with this it would
    // be easy to merge Rule 2 with Rule 3&4 if we wanted to.
    FilterBigCCs->Run(size);
    int big_ccs = radix_sort->FilterIds(size, gpu->Locations, gpu->Max_index);

    radix_sort->ClearFlags(cc_size, gpu->SmallBuffer);
    radix_sort->ClearFlags(cc_size * cc_size, gpu->Min_index);
    if (big_ccs > 0)
    {
#if 1
        // O(n) time, O(kn) work.
        // Finds the neighboring vertex u for each CC, if it is there.
        FindNeighboringVertex->SetArgument(0, cc_size);
        FindNeighboringVertex->Run(big_ccs);
        GetUCount->SetArgument(0, cc_size);
        GetUCount->Run(cc_size);
#else
        // O(log n) time, O(m) work.
        // Flag all the edges with the flags of their endpoints.
        FlagUEdges->Run(adj_size);

        // Sort these values.
        gpu->WriteBackVertices(size, gpu->Vertices, gpu->Neighbor_prefix_sum);
        gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Local_totals);
        radix_sort->InitNeighborSort(adj_size, gpu->Neighbor_prefix_sum, gpu->Local_totals, gpu->Adj_list);
        radix_sort->Sort(adj_size, bits_per_cc - 1, gpu->SmallBuffer);

        // Use the min_values of the radix sort to find out if the
        // vertex is connected to more than (|K| + |N(K)|)/2 nbs.
        radix_sort->ClearFlags(cc_size, gpu->Neighbor_prefix_sum);
        FindNeighboringVertex2->Run(adj_size);
        gpu->WriteBackValues(cc_size, gpu->SmallBuffer, gpu->Neighbor_prefix_sum);
#endif
    }

    // Flag the edges that will be removed from the graph.
    FlagRemoveEdges->Run(adj_size);

    // Calculate the number of edges between u and K.
    radix_sort->CalculatePrefixSum(adj_size, gpu->Output);
    int connected_u_edges = radix_sort->GetLargestPrefixSumValue(adj_size);

    // Filter the removed edges from the graph.
    int remove_edits = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Min_index, gpu->Output) / 2;
    int total_edits = remove_edits + (add_edits + remove_edits + connected_u_edges) / 2;
    if (print && total_edits > 0)
    {
        PrintEdits(remove_edits * 2, size);
    }

    return total_edits;
}

int Rule34::EditGraph(int adj_size, int size)
{
    // First remove the edges that we found.
    int edits = FilterGraph(adj_size, size);
    adj_size -= edits;

    // Get the original order of the verticesn and neighbor_counts.
    gpu->WriteBackVertices(size, gpu->Neighbor_counts, gpu->Max_index);
    gpu->WriteBackVertices(size, gpu->Vertices, gpu->Output);

    // Flag the edges that are connected to u.
    radix_sort->ClearFlags(size, gpu->Local_prefixsum); // size should be cc_size.
    radix_sort->ClearFlags(size, gpu->Locations);
    FindBiggestNeighborU->Run(adj_size);

#if TEST
    // Tests whether the flags of the neighbors of a vertex are
    // all the same.
    TestCCFlags->Run(size);
    gpu->FinishCL();
    if (gpu->GetReturnValue() == -1)
    {
        std::cout << "Oops, something went wrong when flagging the CCs!" << std::endl;
    }
#endif
    // Updates the pointers and set a flag for all the edges that
    // can now be deleted from memory.
    AdjustPointers3->Run(size);

    // Flag the edges where no vertex is pointing at anymore.
    radix_sort->ClearFlags(adj_size, gpu->Min_index);
    SetRemoveFlags3->Run(adj_size);

    // Filter out all the remaining edges that can be removed.
    int inner_edits = FilterGraph(adj_size, size);

    // Update the Critical Cliques.
    if (edits > 0)
    {
        UpdateEditingDegree(adj_size, size);
        UpdateEditingDegreeVertices(adj_size - inner_edits, size);
    }
    return edits + inner_edits;
}

void Rule34::UpdateEditingDegree(int adj_size, int size)
{
    // set editing degree of edges not connected to a vert that is
    // connected to u to 1. all other the editing degree will be 0.
    // for edges that are connected to u we have to count the nr of nb
    // that are connected to u.
    UpdateEditingDegrees->SetArgument(0, cc_size);
    UpdateEditingDegrees->Run(adj_size);
    // TODO: update the other edges connected to u
}
