#include "../../shared.h"

// Flags the vertices based on rule 2.
__kernel void FlagEditingDegree2(global uint *editing_degree,
                                 global uint *cc_indices, global uint *cc_sizes,
                                 global uint *cc_counts,
                                 global ushort *adj_list, global uint *flags) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int cc_id = cc_indices[vertex];
  int size = cc_sizes[vertex];
  int neighbor_size = cc_counts[cc_id - 1] - size;
  if (vertex != 0 && (size + neighbor_size > editing_degree[vertex]) &&
      size >= neighbor_size) {
    flags[neighbor] = cc_id;
  }
}

// Flag all vertices that have a flag that is the same as their cc_index.
__kernel void SetFlaggedCCFlags(global uint *vert_flags, global uint *cc_counts,
                                global uint *cc_indices, global uint *output) {
  int id = get_global_id(0);
  int cc_id = cc_indices[id];
  output[id] = (vert_flags[id] & 0x0000ffff) == cc_id && id != 0 &&
               cc_counts[cc_id - 1] > 1;
}
