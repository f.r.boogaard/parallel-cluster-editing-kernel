#include "rule2.h"

int Rule2::GetEdits(int k, int adj_size, int size, int cc_size, bool print)
{
    // Flag the vertices that are adjacent to a CC for which the rule applies.
    radix_sort->ClearFlags(size, gpu->Neighbor_values);
    FlagEditingDegree2->Run(adj_size);

    SetRemoveFlags->Run(adj_size);

    // This has to be calculated before the remove_edits, otherwise gpu->Output
    // wont have the correct values.
    int add_edits = GetTotalNeighborSizeDifference(size, adj_size);

    // Find the edges that should be removed.
    int remove_edits = radix_sort->Filter(adj_size, gpu->Adj_list, gpu->Min_index, gpu->Output) / 2;
    int total_edits = remove_edits + (add_edits + remove_edits) / 2;

    if (print && total_edits > 0)
    {
        PrintEdits(remove_edits * 2, size);
    }
    return total_edits;
}

int Rule2::EditGraph(int adj_size, int size)
{
    return Rule1::EditGraph(adj_size, size);
}
