#include "../rule1/rule1.h"

class Rule2 : public Rule1
{
public:
    Rule2(GPU *gpu, RadixSort *radix_sort) : Rule1(gpu, radix_sort)
    {
        FlagEditingDegree2 = new Kernel("src/rules/rule2/rule2.cl", "FlagEditingDegree2");
        FlagEditingDegree2->SetArgument(0, gpu->EditingDegree);
        FlagEditingDegree2->SetArgument(1, gpu->CC_indices);
        FlagEditingDegree2->SetArgument(2, gpu->CC_sizes);
        FlagEditingDegree2->SetArgument(3, gpu->CC_counts);
        FlagEditingDegree2->SetArgument(4, gpu->Adj_list);
        FlagEditingDegree2->SetArgument(5, gpu->Neighbor_values);

        SetFlaggedCCFlags = new Kernel("src/rules/rule2/rule2.cl", "SetFlaggedCCFlags");
        SetFlaggedCCFlags->SetArgument(0, gpu->Neighbor_values);
        SetFlaggedCCFlags->SetArgument(1, gpu->CC_counts);
        SetFlaggedCCFlags->SetArgument(2, gpu->CC_indices);
        SetFlaggedCCFlags->SetArgument(3, gpu->Locations);
    };
    ~Rule2()
    {
        delete FlagEditingDegree2;
        delete SetRemoveFlags;
        delete SetCliqueRemoveFlags;
        delete SetVertexRemoveFlags;
    };
    // Return the number of edits the rule found. If `output` is true these
    // edits are written to std::cout.
    int GetEdits(int k, int adj_size, int size, int cc_size, bool print);
    // Edits the graph in GPU memory according to the edits found
    // with the GetEdits function. This function will also remove/add
    // other edges from the graph that are redundant.
    int EditGraph(int adj_size, int size);

private:
    Kernel *FlagEditingDegree2;
    Kernel *SetFlaggedCCFlags;
};
