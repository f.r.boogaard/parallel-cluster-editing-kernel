#include "precomp.h"

#include "graph.h"
#include <string>

int parse_k(std::string file_location)
{
    std::size_t found = file_location.find_last_of("k");
    if (found == 0)
        return 2000;
    int i = 2;
    std::string k = "";
    while (true)
    {
        k += file_location[found + i++];
        if (file_location[found + i] == '/')
        {
            break;
        }
    }
    return std::stoi(k);
}

int main(int argc, char *argv[])
{
    if (argc == 2)
    {
        std::cout << argv[1] << std::endl;
        int initial_k = parse_k((std::string)argv[1]);
        int total_edits = 0;
        bool print = PRINT_OUTPUT;
        bool editing_degree = false;
        Graph *g = new Graph(argv[1], false);

        Time t1 = getCurrentTime();
        g->SortNeighbors();
        g->SortVertices();
        g->GetEditingDegree();
        bool stop = false;
        while (total_edits < initial_k)
        {
            // g->PrintAdjList();
            int edits = 0;
            for (int i = 1; i < 4; i++)
            {
                // Find the edits.
                int rule_edits = g->FindRuleEdits(initial_k - total_edits, i, print);
                edits += rule_edits;
                total_edits += rule_edits;

                // Edit the graph. Stop when total_edits > initial or
                // when the graph is empty.
                if (!g->FilterGraph(i) || total_edits >= initial_k)
                {
                    stop = true;
                    break;
                }

                // Do rule 5 when the others did not find any edits.
                if (edits == 0 && i == 3)
                {
                    rule_edits = g->FindRuleEdits(initial_k - total_edits, 5, print);
                    edits += rule_edits;
                    total_edits += rule_edits;
                    if ((rule_edits > 0 && !g->FilterGraph(5)) || total_edits >= initial_k)
                    {
                        stop = true;
                        break;
                    }
                }
            }
            // Stop if we did not find any edits.
            if (edits <= 0 || stop)
                break;
        }
        // g->PrintAdjList();
        g->CloseOutputFile(total_edits, getElapsedTime(t1), initial_k);
    }
    return 0;
}
