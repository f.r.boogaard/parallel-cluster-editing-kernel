#include "radix-sort.h"

RadixSort::RadixSort(GPU *Gpu)
{
    gpu = Gpu;
    CalculateLocalPrefixSum = new Kernel("src/radix-sort/radix-sort.cl", "CalculateLocalPrefixSum");
    CalculateLocalPrefixSum->SetArgument(1, gpu->Neighbor_values);
    CalculateLocalPrefixSum->SetArgument(2, gpu->Local_totals);
    CalculateLocalPrefixSum->SetArgument(3, gpu->Prefix_sum);
    CalculateLocalPrefixSum->SetArgument(4, sizeof(uint) * LOCAL_WORK_SIZE, NULL);

    CalculateGlobalPrefixSum = new Kernel("src/radix-sort/radix-sort.cl", "CalculateGlobalPrefixSum");
    CalculateGlobalPrefixSum->SetArgument(2, gpu->Local_prefixsum);
    CalculateGlobalPrefixSum->SetArgument(3, gpu->Prefix_sum);

    LocalRadixPrefixSum = new Kernel("src/radix-sort/radix-sort.cl", "LocalRadixPrefixSum");
    LocalRadixPrefixSum->SetArgument(3, gpu->Prefix_sum);
    LocalRadixPrefixSum->SetArgument(4, gpu->Min_index);
    LocalRadixPrefixSum->SetArgument(5, gpu->Local_totals);
    LocalRadixPrefixSum->SetArgument(6, sizeof(uint) * LOCAL_WORK_SIZE, NULL);

    GlobalRadixPrefixSum = new Kernel("src/radix-sort/radix-sort.cl", "GlobalRadixPrefixSum");
    GlobalRadixPrefixSum->SetArgument(1, gpu->Prefix_sum);
    GlobalRadixPrefixSum->SetArgument(2, gpu->Min_index);
    GlobalRadixPrefixSum->SetArgument(3, gpu->Local_totals);

    CorrectShift = new Kernel("src/radix-sort/radix-sort.cl", "CorrectShift");
    CorrectShift->SetArgument(2, gpu->Output);
    CorrectShift->SetArgument(3, gpu->Prefix_sum);
    CorrectShift->SetArgument(4, gpu->Min_index);

    CorrectShift2 = new Kernel("src/radix-sort/radix-sort.cl", "CorrectShift2");
    CorrectShift2->SetArgument(2, gpu->Output);
    CorrectShift2->SetArgument(3, gpu->Prefix_sum);
    CorrectShift2->SetArgument(4, gpu->Min_index);
    CorrectShift2->SetArgument(5, gpu->Locations);
    CorrectShift2->SetArgument(6, gpu->Max_index);

    SortValues = new Kernel("src/radix-sort/radix-sort.cl", "SortValues");
    SortValues->SetArgument(1, gpu->Prefix_sum); // Can be any buffer that does not have a use when the kernel is run.
    SortValues->SetArgument(2, gpu->Output);

    FilterList = new Kernel("src/radix-sort/filter.cl", "FilterList");
    FilterList->SetArgument(2, gpu->Prefix_sum);
    FilterList->SetArgument(4, gpu->Return_values);

    ReverseFilter = new Kernel("src/radix-sort/filter.cl", "ReverseFilter");
    ReverseFilter->SetArgument(2, gpu->Prefix_sum);
    ReverseFilter->SetArgument(4, gpu->Return_values);

    FilterList16 = new Kernel("src/radix-sort/filter.cl", "FilterList16");
    FilterList16->SetArgument(2, gpu->Prefix_sum);
    FilterList16->SetArgument(4, gpu->Return_values);

    ClearFlag = new Kernel("src/radix-sort/filter.cl", "ClearFlags");

    FilterFlagIds = new Kernel("src/radix-sort/filter.cl", "FilterFlagIds");
    FilterFlagIds->SetArgument(1, gpu->Prefix_sum);
    FilterFlagIds->SetArgument(3, gpu->Return_values);

    GetValue = new Kernel("src/radix-sort/filter.cl", "GetValue");
    GetValue->SetArgument(2, gpu->Return_values);

    InitNbSort = new Kernel("src/radix-sort/radix-sort.cl", "InitNeighborSort");
    InitNbSort->SetArgument(3, gpu->Min_index);
}

void RadixSort::CalculatePrefixSum(int size, Buffer *buffer)
{
    // Calculate a local prefix sum over the buffer values.
    int global_size = size + (LOCAL_WORK_SIZE - (size % LOCAL_WORK_SIZE));
    CalculateLocalPrefixSum->SetArgument(0, size);
    CalculateLocalPrefixSum->SetArgument(1, buffer);
    CalculateLocalPrefixSum->SetArgument(2, gpu->Local_totals);
    CalculateLocalPrefixSum->SetArgument(3, gpu->Prefix_sum);
    CalculateLocalPrefixSum->Run(LOCAL_WORK_SIZE, global_size);
    int totals_size = size / LOCAL_WORK_SIZE;
    int i = LOCAL_WORK_SIZE;
    bool swap = false;
    bool test = false; // TODO: clean up some of this code below.

    // Calculate a prefix sum of the Local_totals values and
    // add these values to the prefix sum of the buffer.
    CalculateGlobalPrefixSum->SetArgument(0, size);
    CalculateLocalPrefixSum->SetArgument(3, gpu->Local_prefixsum); // We will store the prefix sums of the Local_totals in this buffer.
    while (totals_size > 0 || test)
    {

        global_size = totals_size + (LOCAL_WORK_SIZE - (totals_size % LOCAL_WORK_SIZE));
        CalculateLocalPrefixSum->SetArgument(0, totals_size);

        // The Local_totals and Output buffer will swap places each time we iterate.
        CalculateLocalPrefixSum->SetArgument(1 + swap, gpu->Local_totals);
        CalculateLocalPrefixSum->SetArgument(2 - swap, gpu->Output);
        CalculateLocalPrefixSum->Run(LOCAL_WORK_SIZE, global_size);
        CalculateGlobalPrefixSum->SetArgument(1, i);
        CalculateGlobalPrefixSum->Run(size);
        totals_size /= LOCAL_WORK_SIZE;
        i *= LOCAL_WORK_SIZE;
        swap = !swap;
        if (test)
            break;
        if (totals_size == 0)
            test = true;
    }
}

void RadixSort::Sort(int size, int bits, Buffer *buffer, Buffer *buffer2)
{
    LocalRadixPrefixSum->SetArgument(0, size);
    GlobalRadixPrefixSum->SetArgument(0, size);

    Kernel *ShiftKernel = buffer2 == NULL ? CorrectShift : CorrectShift2;

    // The global size has to be a multiple of LOCAL_WORK_SIZE.
    int global_size = size + (LOCAL_WORK_SIZE - (size % LOCAL_WORK_SIZE));
    bool swap = false;
    for (int i = bits; i >= 0; i--)
    {
        LocalRadixPrefixSum->SetArgument(1, 1 << i);
        LocalRadixPrefixSum->SetArgument(2, swap ? gpu->Output : buffer);
        LocalRadixPrefixSum->Run(LOCAL_WORK_SIZE, global_size);
        GlobalRadixPrefixSum->Run(LOCAL_WORK_SIZE, global_size);

        ShiftKernel->SetArgument(0, 1 << i);
        ShiftKernel->SetArgument(1 + swap, buffer);
        ShiftKernel->SetArgument(2 - swap, gpu->Output);
        // These two arguments only have to be set when there is a second buffer.
        ShiftKernel->SetArgument(5 + swap, buffer2);
        ShiftKernel->SetArgument(6 - swap, gpu->Max_index);
        ShiftKernel->Run(size);

        swap = !swap;
    }
    if (swap)
    {
        gpu->WriteBackValues(size, buffer, gpu->Output);
        if (buffer2 != NULL)
            gpu->WriteBackValues(size, buffer2, gpu->Max_index);
    }
}

int RadixSort::Filter(int size, Buffer *list, Buffer *flags, Buffer *output, bool inverse)
{
    Kernel *filter = inverse ? ReverseFilter : FilterList;

    CalculatePrefixSum(size, flags);
    filter->SetArgument(0, flags);
    filter->SetArgument(1, list);
    filter->SetArgument(3, output);
    filter->Run(size);
    return gpu->GetReturnValue();
}

int RadixSort::Filter16(int size, Buffer *list, Buffer *flags, Buffer *output)
{
    CalculatePrefixSum(size, flags);
    FilterList16->SetArgument(0, flags);
    FilterList16->SetArgument(1, list);
    FilterList16->SetArgument(3, output);
    FilterList16->Run(size);
    return gpu->GetReturnValue();
}

int RadixSort::FilterIds(int size, Buffer *flags, Buffer *output)
{
    CalculatePrefixSum(size, flags);
    FilterFlagIds->SetArgument(0, flags);
    FilterFlagIds->SetArgument(2, output);
    FilterFlagIds->Run(size);
    return gpu->GetReturnValue();
}

void RadixSort::ClearFlags(int size, Buffer *flags)
{
    ClearFlag->SetArgument(0, flags);
    ClearFlag->Run(size);
}

int RadixSort::GetLargestPrefixSumValue(int size)
{
    GetValue->SetArgument(0, size - 1);
    GetValue->SetArgument(1, gpu->Prefix_sum);
    GetValue->Run(1);
    return gpu->GetReturnValue();
}

void RadixSort::InitNeighborSort(int size, Buffer *verts, Buffer *counts, Buffer *adj_list)
{
    InitNbSort->SetArgument(0, verts);
    InitNbSort->SetArgument(1, counts);
    InitNbSort->SetArgument(2, adj_list);
    InitNbSort->Run(size);
}

RadixSort::~RadixSort()
{
    delete CalculateLocalPrefixSum;
    delete CalculateGlobalPrefixSum;
    delete LocalRadixPrefixSum;
    delete GlobalRadixPrefixSum;
    delete CorrectShift;
    delete SortValues;
    delete FilterList;
    delete ReverseFilter;
    delete ClearFlag;
}