#include "../shared.h"

// Filters out all elements that have a flag.
__kernel void FilterList(global uint *flags, global uint *list,
                         global uint *prefix_sum, global uint *output,
                         global uint *return_values) {
  int id = get_global_id(0);
  if (flags[id]) {
    output[prefix_sum[id] - 1] = list[id];
  }
  if (id == get_global_size(0) - 1) {
    // Return the number of elements that are removed to the CPU.
    return_values[0] = prefix_sum[id];
  }
}

// Filters out all elements that dont have a flag.
__kernel void ReverseFilter(global uint *flags, global uint *list,
                            global uint *prefix_sum, global uint *output,
                            global uint *return_values) {
  int id = get_global_id(0);
  if (!flags[id]) {
    output[id - prefix_sum[id]] = list[id];
  }
  if (id == get_global_size(0) - 1) {
    // Return the number of elements that are removed to the CPU.
    return_values[0] = prefix_sum[id];
  }
}

// Filter out all elements with a flag and put the id as value.
__kernel void FilterFlagIds(global uint *flags, global uint *prefix_sum,
                            global uint *output, global uint *return_values) {
  int id = get_global_id(0);
  if (flags[id]) {
    output[prefix_sum[id] - 1] = id;
  }
  if (id == get_global_size(0) - 1) {
    return_values[0] = prefix_sum[id];
  }
}

// Filters out all elements that have a flag. Only uses the first 16 bits of the
// prefix_sum.
__kernel void FilterList16(global uint *flags, global uint *list,
                           global uint *prefix_sum, global uint *output,
                           global uint *return_values) {
  int id = get_global_id(0);
  if (flags[id]) {
    int index = (prefix_sum[id] & 0x0000ffff) - 1;
    output[index] = list[id];
  }
  if (id == get_global_size(0) - 1) {
    // Return the number of elements that are removed to the CPU.
    return_values[0] = prefix_sum[id];
  }
}

__kernel void ClearFlags(global uint *flags) { flags[get_global_id(0)] = 0; }

// Writes a value from a buffer to the return_values buffer, so that
// you can get the value back to the CPU.
__kernel void GetValue(int id, global uint *buffer, global int *return_values) {
  return_values[0] = buffer[id];
}
