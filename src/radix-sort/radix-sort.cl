#include "../shared.h"

__kernel void InitNeighborSort(global uint *vertices,
                               global uint *neighbor_counts,
                               global ushort *adj_list,
                               global uint *min_indices) {
  int id = get_global_id(0);
  int vertex = adj_list[id * 2 + 1];
  int start = vertices[vertex];
  int end = (start + (neighbor_counts[vertex] & 0x0000ffff) - 1) - id;
  min_indices[id] = (id - start) | (end << (MAX_BITS / 2));
}

void calculateLocalPrefixSum(int totals_id, int min_index, int value,
                             global uint *local_totals,
                             global uint *global_prefix_sum,
                             local uint *local_prefix_sum) {
  int gid = get_global_id(0);
  int lid = get_local_id(0);
  local_prefix_sum[lid] = value;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int j = 1; gid - min_index >= j; j <<= 1) {
    uint a = local_prefix_sum[lid - j];
    barrier(CLK_LOCAL_MEM_FENCE);
    local_prefix_sum[lid] += a;
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  global_prefix_sum[gid] = local_prefix_sum[lid];
  if (lid == get_local_size(0) - 1) {
    local_totals[totals_id] = local_prefix_sum[lid];
  }
}

__kernel void CalculateLocalPrefixSum(int size, global uint *values,
                                      global uint *local_totals,
                                      global uint *global_prefix_sum,
                                      local uint *local_prefix_sum) {
  int id = get_global_id(0);
  if (id < size) {
    int local_size = get_local_size(0);
    int totals_id = id / local_size;
    calculateLocalPrefixSum(totals_id, totals_id * local_size, values[id],
                            local_totals, global_prefix_sum, local_prefix_sum);
  }
}

// Supports up to 2^MAX_BITS vertices, where MAX_BITS < sizeof(int) * 4.
__kernel void LocalRadixPrefixSum(int size, int bits, global uint *values,
                                  global uint *global_prefix_sum,
                                  global uint *min_indices,
                                  global uint *local_totals,
                                  local uint *local_prefix_sum) {
  int gid = get_global_id(0);
  if (gid < size) {
    uint local_size = get_local_size(0);
    uint totals_id = gid / local_size;
    int min_index =
        max(gid - (min_indices[gid] & 0x0000ffff), totals_id * local_size);

    // The prefix sum of the 0s is done in the first half of the bits and the
    // prefix sum of the 1s is done in the second half of the bits. This means
    // that a vertex cannot have more than 2^16 neighbors, otherwise this wont
    // work.
    int bit = (values[gid] & bits) != 0;
    bit |= ((bit ^ 1) << (MAX_BITS / 2));
    calculateLocalPrefixSum(totals_id, min_index, bit, local_totals,
                            global_prefix_sum, local_prefix_sum);
  }
}

// quadratic version
void calculateGlobalPrefixSum(int size, int min_index,
                              global uint *local_totals,
                              global uint *global_prefix_sum) {
  int id = get_global_id(0);
  if (id < size) {
    int local_size = get_local_size(0);
    int local_min = id / local_size;
    int min_diff = local_min - (min_index / local_size);
    int prefix = 0;
    for (int i = 1; i <= min_diff; i++) {
      prefix += local_totals[local_min - i];
    }
    global_prefix_sum[id] += prefix;
  }
}

// linear version
void calculateGlobalPrefixSum2(int size, int min_index, int offset,
                               global uint *local_totals,
                               global uint *global_prefix_sum) {
  int id = get_global_id(0);
  if (id < size) {
    if (id % (offset * LOCAL_WORK_SIZE) >= offset) {
      global_prefix_sum[id] += local_totals[(id / offset) - 1];
    }
  }
}

__kernel void CalculateGlobalPrefixSum(int size, int offset,
                                       global uint *local_totals,
                                       global uint *global_prefix_sum) {
  calculateGlobalPrefixSum2(size, 0, offset, local_totals, global_prefix_sum);
}

__kernel void GlobalRadixPrefixSum(int size, global uint *global_prefix_sum,
                                   global uint *min_indices,
                                   global uint *local_totals) {
  int id = get_global_id(0);
  calculateGlobalPrefixSum(size, id - (min_indices[id] & 0x0000ffff),
                           local_totals, global_prefix_sum);
}

inline int getShiftedPrefixSumValue(int id, int bits, global uint *values,
                                    global uint *min_indices,
                                    global uint *prefix_sum) {
  int min_index = id - (min_indices[id] & 0x0000ffff);
  int max_index = id + (min_indices[id] >> (MAX_BITS / 2));
  int shift = prefix_sum[max_index] & 0x0000ffff;
  // Update the min and max values.
  if (id < shift + min_index) {
    min_indices[id] -= (max_index - (min_index + shift - 1)) << (MAX_BITS / 2);
  } else {
    min_indices[id] -= shift;
  }
  // Correct the prefix_sum values.
  if ((values[id] & bits) != 0) {
    return (prefix_sum[id] & 0x0000ffff) + min_index - 1;
  } else {
    return (prefix_sum[id] >> (MAX_BITS / 2)) + shift + min_index - 1;
  };
}

// Assigns the correct values to the prefix sum by adding the shift to the 0
// values.
__kernel void CorrectShift(int bits, global uint *values, global uint *output,
                           global uint *prefix_sum, global uint *min_indices) {
  // Get the amount of places you have to shift by.
  int id = get_global_id(0);
  int loc = getShiftedPrefixSumValue(id, bits, values, min_indices, prefix_sum);
  output[loc] = values[id];
}

// Assigns the correct values to the prefix sum by adding the shift to the 0
// values.
__kernel void CorrectShift2(int bits, global uint *values, global uint *output,
                            global uint *prefix_sum, global uint *min_indices,
                            global uint *locations, global uint *loc_output) {
  // Get the amount of places you have to shift by.
  int id = get_global_id(0);
  int loc = getShiftedPrefixSumValue(id, bits, values, min_indices, prefix_sum);
  output[loc] = values[id];
  loc_output[loc] = locations[id];
}

// Put the values in a new order according to the prefix sum.
// In order to prevent mix ups, we first write to an output array.
__kernel void SortValues(global uint *values, global uint *output,
                         global uint *prefix_sum) {
  int id = get_global_id(0);
  output[prefix_sum[id]] = values[id];
}

// Write the values back to the value array.
__kernel void WriteOutput(global uint *values, global uint *output) {
  int id = get_global_id(0);
  values[id] = output[id];
}
