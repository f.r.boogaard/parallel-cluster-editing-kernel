#include "../precomp.h"
#include "../GPU.h"

class RadixSort
{
public:
    RadixSort(GPU *gpu);
    ~RadixSort();

    // Performs a radix sort on `buffer`. Also sorts `buffer2` in the same way.
    void Sort(int size, int bits, Buffer *buffer, Buffer *buffer2 = NULL);
    // Calculates a prefix sum of the buffer and stores the result in the Prefix_sum buffer.
    void CalculatePrefixSum(int size, Buffer *buffer);
    // Calculates a prefix sum with ranges stored in the min and max buffers.
    void CalculateRangePrefixSum(int size, Buffer *buffer);
    // Filters `list` based on the `flags`. The resulting list will be
    // written to `output`. Returns the number of elements that were removed.
    int Filter(int size, Buffer *list, Buffer *flags, Buffer *output, bool inverse = false);
    // Filters the flags and puts the ids as value.
    int FilterIds(int size, Buffer *flags, Buffer *output);
    // Same as Filter, but uses only the first 16 bits of the prefix sum.
    int Filter16(int size, Buffer *list, Buffer *flags, Buffer *output);
    // Sets all values of the flag buffer to 0.
    void ClearFlags(int size, Buffer *flags);
    // Returns the last element of the prefix sum.
    int GetLargestPrefixSumValue(int size);
    // Initializes the min_values for when we want to sort the neighbors.
    void InitNeighborSort(int size, Buffer *verts, Buffer *counts, Buffer *adj_list);

private:
    GPU *gpu;

    Kernel *CalculateLocalPrefixSum;
    Kernel *CalculateGlobalPrefixSum;
    Kernel *LocalRadixPrefixSum;
    Kernel *GlobalRadixPrefixSum;
    Kernel *CorrectShift;
    Kernel *CorrectShift2;
    Kernel *SortValues;
    Kernel *InitNbSort;

    Kernel *FilterList;
    Kernel *ReverseFilter;
    Kernel *ClearFlag;
    Kernel *FilterFlagIds;
    Kernel *FilterList16;

    Kernel *GetValue;
};
