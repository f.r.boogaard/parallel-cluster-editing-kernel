#include "precomp.h"
using namespace std::chrono;

typedef std::chrono::_V2::steady_clock::time_point Time;

static Time getCurrentTime()
{
    return steady_clock::now();
}

static double getElapsedTime(Time t1)
{
    return duration_cast<duration<double>>(steady_clock::now() - t1).count();
}
