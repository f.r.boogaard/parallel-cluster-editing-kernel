#include "precomp.h"

class GPU
{
public:
    // Array connected to the Output buffer. Can be used to retieve
    // data from the gpu if used in combination with WriteBackValues.
    uint *output;

    // Contains the start index for each vertex in the Adj_list.
    Buffer *Vertices;
    // Contains the vertex id and the degree of the vertex.
    Buffer *Neighbor_counts;
    // Contains all the edges in the graph.
    Buffer *Adj_list;
    // All prefix sums will be calculated in this buffer.
    Buffer *Prefix_sum;
    // Contains the min index in the Adj_list for each edge.
    Buffer *Min_index;
    // Contains the max index in the Adj_list for each edge.
    Buffer *Max_index;
    // Buffer that will contain the results of the local prefix sums.
    Buffer *Local_totals;
    // Buffer that is used to store the local prefix sums in.
    Buffer *Local_prefixsum;
    // Buffer that can be used to write output to.
    Buffer *Output;
    // Can contains values for each vertex in the graph, for example an integer
    // representation of the first few neighbors.
    Buffer *Neighbor_values;
    // Another prefix sum buffer, but this one can only handle up to n elements.
    Buffer *Neighbor_prefix_sum;
    // A small buffer we can use to retreive data from the GPU.
    Buffer *Return_values;
    // Locations contains the original location of the vertices before the radix sort.
    // We will use these values to sort the Neighbor counts and vertices in one go,
    // instead of moving them each iteration of the Radix Sort.
    Buffer *Locations;
    // Buffer that contains the CC size of the CC that the vertex is in.
    Buffer *CC_sizes;
    // Buffer that contains the CC index of the CC that the vertex is in.
    Buffer *CC_indices;
    // Contains the neighbor counts for the CCs.
    Buffer *CC_counts;
    // Contains index pointers to the Adj_list for the CCs.
    Buffer *Critical_cliques;
    // Small buffer. Just a buffer that can be used for anything.
    Buffer *SmallBuffer;
    // Contains (once calculated) the editing degree for each vertex.
    Buffer *EditingDegree;
    // Contains pointers for each edge to an editing degree.
    Buffer *EditingDegreeEdges;

    GPU(int size, int adj_size);
    ~GPU();

    // Copies all data to the GPU.
    void CopyBuffersToDevice();
    // Copies the first `size` values from `buffer2` to `buffer1`.
    void WriteBackValues(int size, Buffer *buffer1, Buffer *buffer2);
    // Puts the values of `buffer` back in the original vertex order.
    // if `write_back` is true, then the buffer is updated with these values
    // otherwise you can find the result in the `output` buffer.
    void WriteBackVertices(int size, Buffer *buffer, Buffer *output, bool write_back = false);
    // Returns the first value in the Return_values buffer.
    int GetReturnValue();

    // Calling this function will pause the execution of the program until the GPU is done.
    void FinishCL() { WriteOutput->FinishCL(); };

    // Prints the values of a buffer to std::cout. NOTE: gpu->Output will be overridden
    // by the values of the buffer. Each value in the buffer will first be masked and
    // with `mask` and then right shifted by `shift` bits.
    void PrintBuffer(Buffer *buffer, int size, int mask = 0xffffffff, int shift = 0);

    void SetEditingDegreeBuffer(int size);

private:
    // Contains values we want to get back from the GPU.
    int *return_values;

    Kernel *WriteOutput;
    Kernel *ResetVertexLocations;
};