#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

// OpenCL
#include "../lib/OpenCL/system.h"

// SIMD
// #include <nmmintrin.h> // SSE 4.2
// #include <immintrin.h> // AVX
// #include "../lib/vectorclass/vectorclass.h" // Vector class of Agner Fogg (https://www.agner.org/optimize/#vectorclass)

#include "shared.h"

typedef unsigned int uint;
