#include "precomp.h"
#include "graph.h"

void Graph::ParseGraph(std::string file_name, bool weighted)
{
    if (weighted)
    {
        std::cout << "Sorry, weighted graphs are not supported (yet).\n";
    }

    std::ifstream file(file_name);
    if (!file)
    {
        std::cout << "Cannot open input file.\n";
        return;
    }
    file >> size;
    bits_per_vertex = 1;
    int v = size;
    while (v >>= 1)
    {
        bits_per_vertex++;
    }
    vertices_per_int = (int)(MAX_BITS / bits_per_vertex);

    // Ignore the next n lines, they contain ids for the vertices.
    // We will use integers from 0-n as id instead.
    std::string str;
    for (int i = 0; i < size + 1; i++)
    {
        std::getline(file, str);
    }

    // The next n lines contain the adjacency matrix
    adj_matrix = new int[size * size];
    memset(adj_matrix, 0, size * size * sizeof(int));
    for (int i = 0; i < size; i++)
    {
        for (int j = i + 1; j < size; j++)
        {
            file >> str;
            if (stof(str) > 0)
            {
                adj_matrix[i * size + j] = j + 1;
                adj_matrix[j * size + i] = i + 1;
            }
        }
    }
    file.close();
    uint *verts = new uint[size + 1];

    // Create a temporary array for the adj_list,
    // since we do not know the exact size yet.
    int list_size = (size + 2) * size + 1;
    uint *adjacency_list = new uint[list_size];
    memset(adjacency_list, 0, list_size * sizeof(uint));
    adj_size = 1;
    verts[0] = 0;
    adjacency_list[0] = 0;
    // Create adjacency lists for each vertex.
    for (int i = 0; i < size; i++)
    {
        verts[i + 1] = adj_size;
        adjacency_list[adj_size++] = i + 1;
        // Count the number of neighbors of vertex i.
        int nbs = 1;
        for (int j = 0; j < size; j++)
        {
            if (adj_matrix[i * size + j] > 0)
            {
                adjacency_list[adj_size++] = j + 1;
                nbs++;
            }
        }
    }

    size++;
#if PRINT
    outfile.open("results/test.csv", std::ios_base::app);
    outfile << file_name << ", ";
#endif
    InitCL(verts, adjacency_list);
}

void Graph::InitCL(uint *verts, uint *adjacency_list)
{
    gpu = new GPU(size, adj_size);

    /////////////
    // Kernels //
    /////////////
    SetDuplicateEdgesFlags = new Kernel("src/kernels/editing-degree.cl", "SetDuplicateEdgesFlags");
    InitVertexSort = new Kernel("src/kernels/vertex-sort.cl", "InitVertexSort");
    LoadNeighborValues = new Kernel("src/kernels/vertex-sort.cl", "LoadNeighborValues");
    LoadDegreeValues = new Kernel("src/kernels/vertex-sort.cl", "LoadDegreeValues");
    OrderVertices = new Kernel("src/kernels/vertex-sort.cl", "OrderVertices");
    WriteVertices = new Kernel("src/kernels/vertex-sort.cl", "WriteVertices");
    SetFilterValues = new Kernel("src/kernels/vertex-sort.cl", "SetFilterValues");
    UpdatePrefixSum = new Kernel("src/kernels/vertex-sort.cl", "UpdatePrefixSum");

    FilterCCs = new Kernel("src/kernels/editing-degree.cl", "FilterCCs");
    SetCCNeighbors = new Kernel("src/kernels/editing-degree.cl", "SetCCNeighbors");
    FilterCCAdjList = new Kernel("src/kernels/editing-degree.cl", "FilterCCAdjList");
    UpdateCCMinValues = new Kernel("src/kernels/editing-degree.cl", "UpdateCCMinValues");
    CalculateCCNeighborCounts = new Kernel("src/kernels/editing-degree.cl", "CalculateCCNeighborCounts");
    SwapCCIdsAndCCSize = new Kernel("src/kernels/editing-degree.cl", "SwapCCIdsAndCCSize");
    CountConnectedNeighbors = new Kernel("src/kernels/editing-degree.cl", "CountConnectedNeighbors");
    CalculateEditingDegree = new Kernel("src/kernels/editing-degree.cl", "CalculateEditingDegree");
    SetVertexEditingDegree = new Kernel("src/kernels/editing-degree.cl", "SetVertexEditingDegree");

    /////////////
    // Buffers //
    /////////////
    adj_list = (uint *)gpu->Adj_list->GetHostPtr();
    memset(adj_list, 0, adj_size * sizeof(uint));
    for (int i = 0; i < size; i++)
    {
        int start = verts[i];
        int end = i < size - 1 ? verts[i + 1] : adj_size;
        for (int j = start; j < end; j++)
        {
            adj_list[j] = adjacency_list[j] | (i << (MAX_BITS / 2));
        }
    }
    delete adjacency_list;

    vertices = (uint *)gpu->Vertices->GetHostPtr();
    for (int i = 0; i < size; i++)
    {
        vertices[i] = verts[i];
    }
    delete verts;

    neighbor_counts = (uint *)gpu->Neighbor_counts->GetHostPtr();
    for (int i = 0; i < size - 1; i++)
    {
        neighbor_counts[i] = (vertices[i + 1] - vertices[i]) | (i << (MAX_BITS / 2));
    }
    neighbor_counts[size - 1] = (adj_size - vertices[size - 1]) | ((size - 1) << (MAX_BITS / 2));

    cc_sizes = (uint *)gpu->CC_sizes->GetHostPtr();
    memset(cc_sizes, 0, size * sizeof(uint));
    cc_indices = (uint *)gpu->CC_indices->GetHostPtr();
    memset(cc_indices, 0, size * sizeof(uint));

    //////////////////
    // Copy buffers //
    //////////////////

    gpu->CopyBuffersToDevice();

    /////////////////////////
    // Vertex Sort Kernels //
    /////////////////////////

    InitVertexSort->SetArgument(0, gpu->Neighbor_prefix_sum);
    InitVertexSort->SetArgument(1, gpu->Min_index);

    LoadNeighborValues->SetArgument(1, vertices_per_int);
    LoadNeighborValues->SetArgument(2, bits_per_vertex);
    LoadNeighborValues->SetArgument(3, gpu->Vertices);
    LoadNeighborValues->SetArgument(4, gpu->Neighbor_counts);
    LoadNeighborValues->SetArgument(5, gpu->Neighbor_values);
    LoadNeighborValues->SetArgument(6, gpu->Adj_list);
    LoadNeighborValues->SetArgument(7, gpu->Neighbor_prefix_sum);
    LoadNeighborValues->SetArgument(8, gpu->Locations);

    LoadDegreeValues->SetArgument(0, gpu->Neighbor_counts);
    LoadDegreeValues->SetArgument(1, gpu->Neighbor_values);
    LoadDegreeValues->SetArgument(2, gpu->Locations);

    OrderVertices->SetArgument(0, gpu->Neighbor_prefix_sum);
    OrderVertices->SetArgument(1, gpu->Locations);
    OrderVertices->SetArgument(2, gpu->Output); // Can be any buffer that does not have a use when the kernel is run.
    OrderVertices->SetArgument(3, gpu->Vertices);
    OrderVertices->SetArgument(4, gpu->Prefix_sum); // Can be any buffer that does not have a use when the kernel is run.
    OrderVertices->SetArgument(5, gpu->Neighbor_counts);

    WriteVertices->SetArgument(0, gpu->Neighbor_prefix_sum);
    WriteVertices->SetArgument(1, gpu->Output); // Can be any buffer that does not have a use when the kernel is run.
    WriteVertices->SetArgument(2, gpu->Vertices);
    WriteVertices->SetArgument(3, gpu->Prefix_sum); // Can be any buffer that does not have a use when the kernel is run.
    WriteVertices->SetArgument(4, gpu->Neighbor_counts);

    SetFilterValues->SetArgument(0, gpu->Neighbor_values);
    SetFilterValues->SetArgument(1, gpu->Min_index);
    SetFilterValues->SetArgument(2, gpu->Neighbor_prefix_sum);
    SetFilterValues->SetArgument(3, gpu->CC_sizes);
    SetFilterValues->SetArgument(4, gpu->CC_indices);

    UpdatePrefixSum->SetArgument(0, gpu->Neighbor_values);
    UpdatePrefixSum->SetArgument(1, gpu->Prefix_sum);
    UpdatePrefixSum->SetArgument(2, gpu->Neighbor_prefix_sum);
    UpdatePrefixSum->SetArgument(3, gpu->Output); // Can be any buffer that does not have a use when the kernel is run.
    UpdatePrefixSum->SetArgument(4, gpu->Min_index);
    UpdatePrefixSum->SetArgument(5, gpu->Locations); // Can be any buffer that does not have a use when the kernel is run.
    UpdatePrefixSum->SetArgument(6, gpu->Return_values);

    ////////////////////////////
    // Editing Degree Kernels //
    ////////////////////////////

    FilterCCs->SetArgument(0, gpu->CC_indices);
    FilterCCs->SetArgument(1, gpu->CC_counts);
    FilterCCs->SetArgument(2, gpu->Vertices);
    FilterCCs->SetArgument(3, gpu->Neighbor_counts);
    FilterCCs->SetArgument(4, gpu->CC_sizes);
    FilterCCs->SetArgument(5, gpu->Locations);
    FilterCCs->SetArgument(6, gpu->Return_values);

    SetCCNeighbors->SetArgument(1, gpu->Max_index);
    SetCCNeighbors->SetArgument(2, gpu->Prefix_sum);
    SetCCNeighbors->SetArgument(3, gpu->Adj_list);
    SetCCNeighbors->SetArgument(4, gpu->CC_counts);
    SetCCNeighbors->SetArgument(5, gpu->Neighbor_prefix_sum);
    SetCCNeighbors->SetArgument(6, gpu->CC_indices);
    SetCCNeighbors->SetArgument(7, gpu->Critical_cliques);
    SetCCNeighbors->SetArgument(8, gpu->Neighbor_values);
    SetCCNeighbors->SetArgument(9, gpu->Return_values);

    FilterCCAdjList->SetArgument(0, gpu->Max_index);
    FilterCCAdjList->SetArgument(1, gpu->Min_index);

    UpdateCCMinValues->SetArgument(0, gpu->Output);
    UpdateCCMinValues->SetArgument(1, gpu->Min_index);

    CalculateCCNeighborCounts->SetArgument(1, gpu->Min_index);
    CalculateCCNeighborCounts->SetArgument(2, gpu->Output);
    CalculateCCNeighborCounts->SetArgument(3, gpu->Max_index);
    CalculateCCNeighborCounts->SetArgument(4, gpu->Neighbor_values);

    SwapCCIdsAndCCSize->SetArgument(0, gpu->Output);
    SwapCCIdsAndCCSize->SetArgument(1, gpu->Locations);

    SetDuplicateEdgesFlags->SetArgument(0, gpu->Output);
    SetDuplicateEdgesFlags->SetArgument(1, gpu->Max_index);

    CountConnectedNeighbors->SetArgument(1, gpu->EditingDegree);
    CountConnectedNeighbors->SetArgument(2, gpu->SmallBuffer);
    CountConnectedNeighbors->SetArgument(3, gpu->Output);
    CountConnectedNeighbors->SetArgument(4, gpu->Locations);
    CountConnectedNeighbors->SetArgument(5, gpu->Min_index);
    CountConnectedNeighbors->SetArgument(6, gpu->Neighbor_values);
    CountConnectedNeighbors->SetArgument(7, gpu->CC_counts);

    CalculateEditingDegree->SetArgument(2, gpu->Min_index);
    CalculateEditingDegree->SetArgument(3, gpu->Neighbor_values);
    CalculateEditingDegree->SetArgument(4, gpu->CC_counts);
    CalculateEditingDegree->SetArgument(5, gpu->Locations);
    CalculateEditingDegree->SetArgument(6, gpu->EditingDegree);
    CalculateEditingDegree->SetArgument(7, gpu->Local_prefixsum);

    SetVertexEditingDegree->SetArgument(0, gpu->Local_prefixsum);
    SetVertexEditingDegree->SetArgument(1, gpu->CC_indices);
    SetVertexEditingDegree->SetArgument(2, gpu->EditingDegree);

    radix_sort = new RadixSort(gpu);
    rule1 = new Rule1(gpu, radix_sort);
    rule2 = new Rule2(gpu, radix_sort);
    rule34 = new Rule34(gpu, radix_sort);
    rule5 = new Rule5(gpu, radix_sort);

#if PRINT
    outfile << size << ", " << adj_size << ", ";
#endif
}
