#include "../shared.h"

// Put the Vertices and Neighbor_counts back into their original place.
__kernel void ResetVertexLocations(global uint *neighbor_counts,
                                   global uint *list, global uint *output) {
  int id = get_global_id(0);
  output[neighbor_counts[id] >> (MAX_BITS / 2)] = list[id];
}

// Initializes some values for the vertex sort algorithm.
__kernel void InitVertexSort(global uint *prefix_sum, global uint *min_values) {
  int id = get_global_id(0);
  prefix_sum[id] = 0;
  min_values[id] = id | (((get_global_size(0) - 1) - id) << (MAX_BITS / 2));
}

// Corrects the Vertices and Neighbor_counts for the edges that
// were removed from the graph. Also update the CC_counts and
// Critical_cliques. We cant update the CC_indices and CC_sizes yet.
__kernel void UpdateNeighborCounts(global uint *adj_list, global uint *vertices,
                                   global uint *neighbor_counts,
                                   global uint *prefix_sum,
                                   global uint *cc_indices,
                                   global uint *cc_counts,
                                   global uint *critical_cliques) {
  int id = get_global_id(0);
  int start = vertices[id];
  int vertex = neighbor_counts[id] >> (MAX_BITS / 2);
  int cc_id = cc_indices[vertex] - 1;
  // number of places the adj is shifted for this vertex.
  int diff = prefix_sum[max(0, start - 1)];
  vertices[id] -= diff;
  int size = neighbor_counts[id] & 0x0000ffff;
  // number of places the adj is shifted for the next vertex.
  int removed = prefix_sum[start + size - 1];
  // the difference of removed and diff will be the amount of
  // neighbors that are removed. We can directly substract it,
  // since it wont influence the last 16 bits with the vertex id.
  neighbor_counts[id] -= removed - diff;
  // if this is the location used for the critical_clique
  // values, we have to update it. In order to make sure we
  // only update the CC_counts once, we do it here as well.
  if (critical_cliques[cc_id] == start) {
    critical_cliques[cc_id] -= diff;
    cc_counts[cc_id] -= removed - diff;
  }
}

// Load the next neighbor values in a single int.
__kernel void
LoadNeighborValues(int offset, int verts_per_int, int bits_per_vertex,
                   global uint *vertices, global uint *neighbor_counts,
                   global uint *neighbor_values, global ushort *adj_list,
                   global uint *prefix_sum, global uint *locations) {
  // `prefix_sum` contains the amount of spots we are ignoring, because they are
  // already in the right place. We have add this value to the id in order to
  // get the right index.
  int id = get_global_id(0);
  int vert_id = id + prefix_sum[id];

  // Load values from `adj_list` into a single int.
  int value = 0;
  int start = vertices[vert_id];
  int end = start + offset + verts_per_int - 1;
  int limit = start + (neighbor_counts[vert_id] & 0x0000ffff);
  for (int i = start + offset; i <= end && i < limit; i++) {
    value |= adj_list[i << 1]
             << ((end - i) * bits_per_vertex); // opt: -= bits_per_vertex
  }
  neighbor_values[id] = value;

  // Reset the locations of the vertices.
  locations[id] = id;
}

__kernel void LoadDegreeValues(global uint *neighbor_counts,
                               global uint *neighbor_values,
                               global uint *locations) {
  int id = get_global_id(0);
  neighbor_values[id] = neighbor_counts[id] & 0x0000ffff;
  locations[id] = id;
}

// Adjusts the values of the radix sort prefix sum to account for
// the values that are already in the correct spot.
__kernel void OrderVertices(global uint *prefix_sum, global uint *locations,
                            global uint *output_vertices, global uint *vertices,
                            global uint *output_counts,
                            global uint *neighbor_counts) {
  int id = get_global_id(0);
  int loc = locations[id] + prefix_sum[id];
  output_vertices[id] = vertices[loc];
  output_counts[id] = neighbor_counts[loc];
}

__kernel void WriteVertices(global uint *prefix_sum,
                            global uint *output_vertices, global uint *vertices,
                            global uint *output_counts,
                            global uint *neighbor_counts) {
  int id = get_global_id(0);
  int loc = id + prefix_sum[id];
  vertices[loc] = output_vertices[id];
  neighbor_counts[loc] = output_counts[id];
}

// Set bits to 1 for the vertices that are already in the correct spot.
// We will calculate a prefix sum of this array and update the array we
// use to correct the positions with the result.
__kernel void SetFilterValues(global uint *neighbor_values,
                              global uint *min_values, global uint *prefix_sum,
                              global uint *cc_sizes, global uint *cc_indices) {
  int id = get_global_id(0);
  int min_index = min_values[id];
  // If min_index == 0, then this is the only place we can
  // put this vertex in.
  int value = min_index == 0 || neighbor_values[id] == 0;
  neighbor_values[id] = value;

  // We can deduce the size of the critical cliques from the min and max values.
  if (value) {
    int index = id + prefix_sum[id];
    int min_v = min_index & 0x0000ffff;
    cc_sizes[index] = 1 + min_v + (min_index >> (MAX_BITS / 2));
    cc_indices[index] = min_v == 0;
  }
}

// Update the prefix_sum values with the prefix sum of the filter values.
// Also set the correct min and max values for the radix sort.
__kernel void
UpdatePrefixSum(global uint *filter_values, global uint *filter_prefix_sum,
                global uint *prefix_sum, global uint *prefix_sum_output,
                global uint *min_values, global uint *min_values_output,
                global uint *return_values) {
  int id = get_global_id(0);
  if (!filter_values[id]) {
    int new_offset = filter_prefix_sum[id];
    int loc = id - new_offset;
    min_values_output[loc] = min_values[id];
    prefix_sum_output[loc] = prefix_sum[id] + new_offset;
  }

  // Set a value, so that the CPU knows how many vertices ended up in the
  // correct spot in this round.
  if (id == get_global_size(0) - 1) {
    return_values[0] = filter_prefix_sum[id];
  }
}
