#include "../shared.h"

__kernel void FilterCCs(global uint *cc_indices, global uint *cc_counts,
                        global uint *vertices, global uint *neighbor_counts,
                        global uint *cc_sizes, global uint *output,
                        global uint *return_values) {
  int id = get_global_id(0);
  int vert_id = neighbor_counts[id] >> (MAX_BITS / 2);
  int nb_count = neighbor_counts[id] & 0x0000ffff;
  // set the sizes of the CCs.
  int cc_id = cc_indices[vert_id];
  cc_counts[cc_id - 1] = nb_count;
  output[cc_id - 1] = cc_sizes[vert_id];

  // Communicate the number of CCs we have back to the CPU. This value will
  // always be on index 0, because the dummy value will always have the biggest
  // cc index.
  if (id == 0)
    return_values[0] = cc_indices[0];
}

// Creates an adj_list for the CCs.
__kernel void
SetCCNeighbors(int cc_size, global uint *cc_adj_list,
               global uint *cc_size_prefix_sum, // prefix sum of the cc sizes
               global ushort *adj_list, global uint *cc_counts,
               global uint *start_values, global uint *cc_indices,
               global uint *critical_cliques, global uint *min_index,
               global uint *return_values) {
  int id = get_global_id(0);
  int neighbor = adj_list[id * 2];
  int vertex = adj_list[id * 2 + 1];
  int cc_index = cc_indices[vertex] - 1;
  int size = cc_counts[cc_index];
  int diff = id - start_values[vertex];
  int index = cc_size_prefix_sum[cc_indices[vertex] - 1] - size;

  // Store the start index of the CC in the count buffer.
  critical_cliques[cc_index] = start_values[vertex];
  min_index[cc_index] = index;

  // Write this neighbor to its correct location in the cc_adj_list.
  cc_adj_list[index + diff] =
      (cc_indices[neighbor] - 1) | (cc_index << (MAX_BITS / 2));

  if (id == 0) {
    return_values[0] = cc_size_prefix_sum[cc_size - 1];
  }
}

__kernel void FilterCCAdjList(global uint *cc_adj_list, global uint *flags) {
  int id = get_global_id(0);
  // Set a flag if the previous edge is the same. Since the cc_adj_list
  // also contains the cc_id itself we will never set a flag if the last
  // edge of a cc happens to be the same as the first of the next cc.
  flags[id] = id == 0 || cc_adj_list[id] != cc_adj_list[id - 1];
}

// After we filtered out the duplicates, we have to update the min values of the
// ccs.
__kernel void UpdateCCMinValues(global ushort *cc_adj_list,
                                global uint *min_values) {
  int id = get_global_id(0);
  int cc_id = cc_adj_list[id * 2 + 1];
  if (id > 0) {
    if (cc_id != cc_adj_list[id * 2 - 1])
      min_values[cc_id] = id;
  } else {
    min_values[cc_id] = 0;
  }
}

// Also replaces the cc_id in the cc_adj_list for the cc_size,
// of the nb so that we can calculate the connected neighbors faster.
__kernel void CalculateCCNeighborCounts(int cc_adj_size,
                                        global uint *min_values,
                                        global uint *cc_adj_list,
                                        global uint *cc_ids,
                                        global uint *output) {
  int id = get_global_id(0);
  int edge = cc_adj_list[id];
  int cc_id = edge >> (MAX_BITS / 2);
  if (id < get_global_size(0) - 1) {
    int cc_id2 = cc_adj_list[id + 1] >> (MAX_BITS / 2);
    if (cc_id != cc_id2)
      output[cc_id] = 1 + id - min_values[cc_id];
  } else {
    output[cc_id] = cc_adj_size - min_values[cc_id];
  }
  // Store the cc_id in a buffer. so we can store
  // the cc_size in the cc_adj_list (see SwapCCIdsAndCCSize kernel).
  cc_ids[id] = cc_id;
}

__kernel void SwapCCIdsAndCCSize(global uint *cc_adj_list,
                                 global uint *cc_sizes) {
  int id = get_global_id(0);
  int nb = cc_adj_list[id] & 0x0000ffff;
  // Store the cc_size in the cc_adj_list.
  cc_adj_list[id] = nb | (cc_sizes[nb] << (MAX_BITS / 2));
}

__kernel void SetDuplicateEdgesFlags(global uint *cc_adj_list,
                                     global uint *cc_ids, global uint *flags) {
  int id = get_global_id(0);
  int neighbor = cc_adj_list[id] & 0x0000ffff;
  int vertex = cc_ids[id];
  flags[id] = vertex < neighbor;
}

// O(k) time, O(k^3) work, for each cc edge we compare the neighbors.
__kernel void
CountConnectedNeighbors(int cc_size, global uint *cc_adj_list,
                        global uint *filtered_adj_list, global uint *cc_ids,
                        global uint *cc_sizes, global uint *min_index,
                        global uint *cc_neighbor_counts, global uint *cc_counts,
                        global uint *uncommon_nbs) {
  int id = get_global_id(0);
  int neighbor = filtered_adj_list[id] & 0x0000ffff;
  int cc_id = cc_ids[id];
  int start1 = min_index[neighbor];
  int start2 = min_index[cc_id];
  int cc_size1 = cc_neighbor_counts[neighbor];
  int cc_size2 = cc_neighbor_counts[cc_id];
  int i = 0;
  int j = 0;
  uint count = 0;
  while (true) {
    if (i >= cc_size1 || j >= cc_size2)
      break;
    uint nb = cc_adj_list[start1 + i];
    ushort a = nb & 0x0000ffff;
    ushort b = cc_adj_list[start2 + j] & 0x0000ffff;
    // We get the size of the nbs CC from the cc_adj_list,
    // this way we save 1 memory acccess (which is the expensive bit here)
    count += (a == b) * (nb >> (MAX_BITS / 2));
    i += (a >= b);
    j += (a <= b);
  }
  // The number of uncommon nbs are the differences of the
  // common neighbors with the sizes added together.
  uint uncommon = (cc_counts[neighbor] - count) + (cc_counts[cc_id] - count);
  uncommon_nbs[neighbor * cc_size + cc_id] = uncommon;
  uncommon_nbs[cc_id * cc_size + neighbor] = uncommon;
}

__kernel void
CalculateEditingDegree(int nr_of_ccs, global uint *connected_count,
                       global uint *min_index, global uint *cc_neighbor_counts,
                       global uint *cc_counts, global uint *cc_sizes,
                       global uint *cc_adj_list, global uint *output) {
  int id = get_global_id(0);
  int size = cc_neighbor_counts[id];
  int start = min_index[id];
  uint cc_size = cc_sizes[id];
  uint cc_count = cc_counts[id];
  uint editing_degree = 0;
  // Sum over the uncommon neighbors of all neighbors.
  for (int i = start; i < start + size; i++) {
    int neighbor = cc_adj_list[i] & 0x0000ffff;
    if (neighbor != id) {
      uint nb_size = cc_sizes[neighbor];
      uint connected = connected_count[id * nr_of_ccs + neighbor];
      editing_degree += connected * nb_size;
    }
  }
  output[id] = editing_degree;
}

// Sets the editing degree for each vertex.
__kernel void SetVertexEditingDegree(global uint *editing_degree,
                                     global uint *cc_indices,
                                     global uint *output) {
  int id = get_global_id(0);
  output[id] = editing_degree[cc_indices[id] - 1];
}
