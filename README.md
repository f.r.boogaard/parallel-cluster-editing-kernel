# Parallel Cluster Editing Kernel

- You need CMake to build this project.
- This project is only tested on Linux. Scripts to build and run work on linux, but also should work if you have gitbash on windows.
- Build the project: ./build.sh
- Run the project: ./run.sh 
- You can change the input file used in the run.sh script.
- If you run ./run.sh [any parameter value], then all files in the data folder will one by one be used as input
- In order to output the edits, you can change the parameter in the shared.h file
- In order to change the file that is used as output file, change rule 79 in parser.h
- You can pretty print the graph, by uncommenting g->PrintAdjList(); in main.cpp
