#include "system.h"
#include <cstdio>

// Math Stuff
// ----------------------------------------------------------------------------
vec3 normalize( const vec3& v ) { return v.normalized(); }
vec3 cross( const vec3& a, const vec3& b ) { return a.cross( b ); }
float dot( const vec3& a, const vec3& b ) { return a.dot( b ); }
vec3 operator * ( const float& s, const vec3& v ) { return vec3( v.x * s, v.y * s, v.z * s ); }
vec3 operator * ( const vec3& v, const float& s ) { return vec3( v.x * s, v.y * s, v.z * s ); }

// FatalError
// Generic error handling; called by FATALERROR macro.
// ----------------------------------------------------------------------------
void FatalError( const char* file, int line, const char* message )
{
	printf("%s, line %i:\n%s", file, line, message );
	exit( 0 );
}

void FatalError( const char* file, int line, const char* message, const char* context )
{
	printf("%s: %s, line %i:\n%s\n", context, file, line, message );
	exit( 0 );
}
